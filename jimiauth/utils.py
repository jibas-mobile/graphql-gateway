from jimiauth.models import JimiToken, JimiUser
from jibasdb.jbsuser.models import Login


def login_pegawai(login: Login) -> str:
    user, _ = JimiUser.objects.get_or_create(
        jibas_login=login.login.nip,
        type=JimiUser.UserType.STAFF,
        defaults={
            'email': login.login.email,
            'type': JimiUser.UserType.STAFF,
        }
    )
    return JimiToken.objects.create(user=user).key