import hashlib
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import LoginPegawaiSerializer

from .utils import login_pegawai
from jibasdb.jbsuser.models import Login


@api_view(['POST'])
def pegawai_auth(request):
    serializer = LoginPegawaiSerializer(data=request.data)
    if not serializer.is_valid():
        return Response({"error": "bad request"}, status=400)

    payload = serializer.validated_data
    try:
        login = Login.objects.get(login=payload['username'])
    except Login.DoesNotExist:
        return Response({"error": "invalid username or password"}, status=401)

    if login.password != hashlib.md5(payload['password'].encode()).hexdigest():
        return Response({"error": "invalid username or password"}, status=401)
    if login.aktif == 0:
        return Response({"error": "user is inactive"}, status=403)

    key = login_pegawai(login)
    return Response({"key": key}, status=201)
