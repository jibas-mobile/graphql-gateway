from rest_framework.authentication import TokenAuthentication

from .models import JimiToken


class JimiTokenAuthentication(TokenAuthentication):
    keyword = 'Bearer'
    model = JimiToken