import pytest


@pytest.mark.django_db
def test_jimiuser():
    from ..models import JimiUser
    user_staff = JimiUser.objects.create(
        jibas_login='123',
        type=JimiUser.UserType.STAFF,
    )
    user_student = JimiUser.objects.create(
        jibas_login='234',
        type=JimiUser.UserType.STUDENT,
    )
    user_parent = JimiUser.objects.create(
        jibas_login='345',
        type=JimiUser.UserType.PARENT,
    )
    assert 'SA_123' == user_staff.username
    assert 'SU_234' == user_student.username
    assert 'PA_345' == user_parent.username
