import pytest


def test_login_invalid_body(client):
    response = client.post('/api/auth/pegawai', data={'ini': 'salah'})
    assert response.status_code == 400


@pytest.mark.django_db
def test_login_invalid_password(client, login_pegawai):
    response = client.post('/api/auth/pegawai', data={
        'username': login_pegawai.login.nip,
        'password': 'salahpasswordnya'
    })
    assert response.status_code == 401, response.content


@pytest.mark.django_db
def test_login_ok(client, login_pegawai):
    response = client.post('/api/auth/pegawai', data={
        'username': login_pegawai.login.nip,
        'password': 'lahaciyak'
    })
    assert response.status_code == 201, response.content
