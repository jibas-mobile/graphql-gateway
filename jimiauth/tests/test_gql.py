import pytest
import json


@pytest.mark.django_db
class TestMe:
    def test_me_not_logged_in(self, client_query):
        response = client_query(
            '''
            query {
                me {
                    email
                    nama
                }
            }
            '''
        )

        assert response.status_code == 401

    def test_me_something(self, login_pegawai):
        assert login_pegawai.replid is not None

    def test_me_ok(self, client_query, jimi_token):
        response = client_query(
            '''
            query {
                me {
                    jibasLogin
                    type
                }
            }
            ''',
            auth_key=jimi_token.key
        )

        content = json.loads(response.content)
        assert 'errors' not in content
        result = content['data']['me']
        assert jimi_token.user.jibas_login == result['jibasLogin']
        assert jimi_token.user.type == result['type']
