import hashlib
import pendulum
import factory
from factory import Faker
from factory.django import DjangoModelFactory
from jibasdb.jbsuser.models import Login
from jibasdb.jbssdm.models import Pegawai, BagianPegawai


class BagianPegawaiAkademikFactory(DjangoModelFactory):
    class Meta:
        model = BagianPegawai
        django_get_or_create = ("replid",)

    replid = 1
    urutan = 1
    ts = pendulum.now()
    token = 123
    issync = 0


class PegawaiFactory(DjangoModelFactory):
    class Meta:
        model = Pegawai
        django_get_or_create = ("nip",)

    nip = Faker('ssn')
    nama = Faker('name')
    email = Faker('email')
    bagian = factory.SubFactory(BagianPegawaiAkademikFactory)
    replid = 1
    ts = pendulum.now()
    token = 123
    issync = 0
    aktif = 1


class PegawaiLoginFactory(DjangoModelFactory):
    class Meta:
        model = Login
        django_get_or_create = ("login",)

    login = factory.SubFactory(PegawaiFactory)
    password = hashlib.md5("lahaciyak".encode()).hexdigest()
    aktif = 1
    replid = 1


class UserFactory(DjangoModelFactory):
    class Meta:
        model = 'jimiauth.JimiUser'
        django_get_or_create = ("jibas_login", "type",)

    # nama = Faker("name")
    # type = 'SA'

    # _peg_login = factory.SubFactory(PegawaiLoginFactory)

    # @factory.lazy_attribute
    # def jibas_login(self):
    #     login = PegawaiLoginFactory()
    #     # return self._peg_login.login.nip
    #     return login.login.nip
    #
    # @factory.lazy_attribute
    # def email(self):
    #     Login.objects.get(pk=self.jibas_login)
    #     return self._peg_login.login.email


class TokenFactory(DjangoModelFactory):
    class Meta:
        model = 'jimiauth.JimiToken'
        django_get_or_create = ("user", "key",)

    # user = factory.SubFactory(UserFactory)
    key = "thisisverytopsecretdonttellpeoplepls"
