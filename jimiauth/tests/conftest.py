import pytest
from graphene_django.utils.testing import graphql_query

from .factories import PegawaiFactory, PegawaiLoginFactory, UserFactory, TokenFactory, BagianPegawaiAkademikFactory
from jibasdb.jbsuser.models import Login
from jibasdb.jbssdm.models import Pegawai, BagianPegawai
from jimiauth.models import JimiUser, JimiToken


@pytest.fixture
def bagian_pegawai_akademik() -> BagianPegawai:
    return BagianPegawaiAkademikFactory()


@pytest.fixture
def pegawai() -> Pegawai:
    return PegawaiFactory()


@pytest.fixture
def login_pegawai() -> Login:
    return PegawaiLoginFactory()


@pytest.fixture
def jimi_user(login_pegawai) -> JimiUser:
    pegawai = login_pegawai.login
    return UserFactory(jibas_login=pegawai.nip,
                       email=pegawai.email,
                       type=JimiUser.UserType.STAFF,
                       nama=pegawai.nama)


@pytest.fixture
def jimi_token(jimi_user) -> JimiToken:
    return TokenFactory(user=jimi_user)


@pytest.fixture
def client_query(client):
    def func(*args, **kwargs):
        headers = kwargs.get('headers', dict())
        auth_key = kwargs.get('auth_key')
        if auth_key is not None:
            headers['HTTP_AUTHORIZATION'] = f'Bearer {auth_key}'
            del kwargs['auth_key']
        return graphql_query(*args, **kwargs, client=client, graphql_url='/graphql',
                             headers=headers)

    return func
