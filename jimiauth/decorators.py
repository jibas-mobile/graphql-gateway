from graphql_extensions import exceptions


def authenticated(func):
    def wrapper(*args, **kwargs):
        user = args[2].context.user
        if not user.is_authenticated:
            raise exceptions.PermissionDenied()
        return func(*args, **kwargs)
    return wrapper