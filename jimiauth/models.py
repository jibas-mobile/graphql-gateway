from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from rest_framework.authtoken.models import Token


class JimiUser(AbstractUser):
    class UserType(models.TextChoices):
        STAFF = 'SA', _('Staff')
        STUDENT = 'SU', _('Student')
        PARENT = 'PA', _('Parent')

    first_name = None
    last_name = None
    REQUIRED_FIELDS = []
    nama = models.CharField(max_length=150, blank=True)
    jibas_login = models.CharField(max_length=150, blank=False)
    type = models.CharField(max_length=2, choices=UserType.choices, db_index=True, default=UserType.STAFF)

    def save(self, *args, **kwargs):
        if not self.username:
            self.username = self.generate_username()
        return super().save(*args, **kwargs)

    def generate_username(self):
        return f'{self.type}_{self.jibas_login}'


class JimiToken(Token):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='auth_tokens',
        on_delete=models.CASCADE, verbose_name=_("User"),
    )
    last_used = models.DateTimeField(_("Last used"), auto_now=True, db_index=True)