import hashlib
import graphene
from jimiauth.models import JimiToken, JimiUser

from jibasdb.jbsuser.models import Login


class LoginType(graphene.Enum):
    STAFF = 1
    STUDENT = 2
    PARENT = 3


class LoginError(graphene.Enum):
    INVALID_USERNAME_PASSWORD = 1
    INACTIVE = 2
    TYPE_UNSUPPORTED = 3


class LoginResult(graphene.Mutation):
    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)
        type = graphene.Argument(LoginType, required=True)

    success = graphene.Boolean(required=True)
    key = graphene.String()
    error = graphene.Field(LoginError)

    @classmethod
    def mutate(cls, root, info, username, password, type):
        if type != LoginType.STAFF:
            return LoginResult(success=False, error=LoginError.TYPE_UNSUPPORTED)
        try:
            login = Login.objects.get(login=username)
        except Login.DoesNotExist:
            return LoginResult(success=False, error=LoginError.INVALID_USERNAME_PASSWORD)
        if login.password != hashlib.md5(password.encode()).hexdigest():
            return LoginResult(success=False, error=LoginError.INVALID_USERNAME_PASSWORD)
        if login.aktif == 0:
            return LoginResult(success=False, error=LoginError.INACTIVE)

        user, _ = JimiUser.objects.get_or_create(
            jibas_login=username,
            type=JimiUser.UserType.STAFF,
            defaults={
                'email': login.login.email,
                'jibas_login': username,
                'type': JimiUser.UserType.STAFF,
            }
        )
        token = JimiToken.objects.create(user=user)

        return LoginResult(success=True, key=token.key)
