import graphene
from graphene_django import DjangoObjectType

from jimiauth.decorators import authenticated
from jimiauth.schema.login import LoginResult
from jimiauth.models import JimiUser
from akademik.schema.akademik.types import PegawaiNode, SiswaNode, Pegawai, Siswa


class UserType(graphene.Union):
    class Meta:
        types = (PegawaiNode, SiswaNode)


class JimiUserNode(DjangoObjectType):
    class Meta:
        model = JimiUser
        fields = (
            "jibas_login",
            "type",
        )
        interfaces = (graphene.relay.Node,)
        convert_choices_to_enum = ["type"]

    jibas_user = graphene.Field(UserType)

    def resolve_jibas_user(cls, root, **kwargs):
        if cls.type == JimiUser.UserType.STAFF:
            return Pegawai.objects.get(pk=cls.jibas_login)
        elif cls.type == JimiUser.UserType.PARENT:
            return Siswa.objects.get(pk=cls.jibas_login)
        return None


class Query(graphene.ObjectType):
    me = graphene.Field(JimiUserNode)
    something = graphene.String()

    @classmethod
    @authenticated
    def resolve_me(cls, root, info):
        return info.context.user


class Mutation(graphene.ObjectType):
    login = LoginResult.Field()
