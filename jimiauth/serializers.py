from rest_framework import serializers


class LoginPegawaiSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()
