import django_filters
import graphene
from django_filters import OrderingFilter
from graphene_django import DjangoObjectType
from django.urls import reverse as django_reverse

from jibasdb.jbsakad.models import Kelas, Departemen, TahunAjaran, Jam, \
    Pelajaran, InfoJadwal, Jadwal, Siswa, Angkatan, Guru, StatusGuru, AsalSekolah, JenisMutasi, Tingkat, KondisiSiswa, \
    StatusSiswa, RiwayatFoto
from jibasdb.jbssdm.models import Pegawai, BagianPegawai
from jibasdb.jbsumum.models import Identitas, TingkatPendidikan, Suku, Agama, JenisPekerjaan
from jibasgql.utils import ExtendedConnection


class JenisMutasiNode(DjangoObjectType):
    class Meta:
        model = JenisMutasi
        name = 'JenisMutasi'
        interfaces = (graphene.relay.Node,)
        fields = (
            'keterangan',
            'info1',
            'info2',
            'info3',
        )

    nama = graphene.String(required=True, source='jenismutasi')
    created_at = graphene.DateTime(required=True, source='ts')


class AngkatanNode(DjangoObjectType):
    class Meta:
        model = Angkatan
        name = 'Angkatan'
        fields = (
            'replid',
            'departemen',
            'aktif',
            'keterangan',
            'info1',
            'info2',
            'info3',
            'ts',
        )
        interfaces = (graphene.relay.Node,)

    nama = graphene.String(source='angkatan', required=True)


class SiswaFilter(django_filters.FilterSet):
    nama = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Siswa
        fields = ['nisn', 'nama', 'nik', 'tahunmasuk', 'idangkatan', 'idkelas']

    order_by = OrderingFilter(
        fields=(
            'nama', 'nisn',
        )
    )


class AsalSekolahNode(DjangoObjectType):
    class Meta:
        model = AsalSekolah
        name = 'AsalSekolah'
        fields = (
            'urutan',
            'info1',
            'info2',
            'info3',
            'ts',
        )
        interfaces = (graphene.relay.Node,)

    nama = graphene.String(required=True, source='sekolah')
    departemen = graphene.String(required=True)
    created_at = graphene.DateTime(required=True, source='ts')


class TingkatPendidikanNode(DjangoObjectType):
    class Meta:
        model = TingkatPendidikan
        name = 'TingkatPendidikan'
        fields = (
            'info1',
            'info2',
            'info3',
            'ts',
        )
        interfaces = (graphene.relay.Node,)

    nama = graphene.String(required=True, source='pendidikan')


class SukuNode(DjangoObjectType):
    class Meta:
        model = Suku
        name = 'Suku'
        fields = (
            'info1',
            'info2',
            'info3',
        )
        interfaces = (graphene.relay.Node,)

    nama = graphene.String(required=True, source='suku')
    created_at = graphene.DateTime(required=True, source='ts')
    urutan = graphene.Int(required=True)


class AgamaNode(DjangoObjectType):
    class Meta:
        model = Agama
        nama = 'Agama'
        fields = (
            'info1',
            'info2',
            'info3',
        )
        interfaces = (graphene.relay.Node,)

    nama = graphene.String(required=True, source='agama')
    created_at = graphene.DateTime(required=True, source='ts')
    urutan = graphene.Int(required=True)


class Kelamin(graphene.Enum):
    LAKILAKI = 'l'
    PEREMPUAN = 'p'


class TingkatNode(DjangoObjectType):
    class Meta:
        model = Tingkat
        name = 'Tingkat'
        fields = (
            'keterangan',
            'info1',
            'info2',
            'info3',
        )
        interfaces = (graphene.relay.Node,)

    nama = graphene.String(required=True, source='tingkat')
    departemen = graphene.Field("akademik.schema.akademik.types.DepartemenNode", required=True)
    aktif = graphene.Boolean(required=True)
    urutan = graphene.Int(required=True)
    created_at = graphene.DateTime(required=True, source='ts')


class KondisiSiswaNode(DjangoObjectType):
    class Meta:
        model = KondisiSiswa
        name = 'KondisiSiswa'
        fields = (
            'info1',
            'info2',
            'info3',
        )
        interfaces = (graphene.relay.Node,)

    nama = graphene.String(required=True, source='kondisi')
    urutan = graphene.Int(required=True)
    created_at = graphene.DateTime(required=True, source='ts')


class StatusSiswaNode(DjangoObjectType):
    class Meta:
        model = StatusSiswa
        name = 'StatusSiswa'
        fields = (
            'info1',
            'info2',
            'info3',
        )
        interfaces = (graphene.relay.Node,)

    nama = graphene.String(required=True, source='status')
    urutan = graphene.Int(required=True)
    created_at = graphene.DateTime(required=True, source='ts')


class JenisPekerjaanNode(DjangoObjectType):
    class Meta:
        model = JenisPekerjaan
        name = 'JenisPekerjaan'
        fields = (
            'info1',
            'info2',
            'info3',
        )
        interfaces = (graphene.relay.Node,)

    nama = graphene.String(required=True, source='pekerjaan')
    urutan = graphene.Int(required=True)
    created_at = graphene.DateTime(required=True, source='ts')


class SiswaNode(DjangoObjectType):
    class Meta:
        model = Siswa
        name = 'Siswa'
        fields = (
            'nisn',
            'nik',
            'noun',
            'replid',
            'panggilan',
            'suku',
            'agama',
            'status',
            'warga',
            'bahasa',
            'berat',
            'tinggi',
            'darah',
            'jarak',
            'kesehatan',
            'kondisi',
            'status',
            'wali',
            'keterangan',
            'hobi',
            'frompsb',
            'ketpsb',
            'info1',
            'info2',
            'info3',
        )
        filterset_class = SiswaFilter
        interfaces = (graphene.relay.Node,)
        connection_class = ExtendedConnection

    nis = graphene.String(required=True)
    nama = graphene.String(required=True)
    kelas = graphene.Field('akademik.schema.akademik.types.KelasNode', source='idkelas', required=True)
    angkatan = graphene.Field('akademik.schema.akademik.types.AngkatanNode', source='idangkatan', required=True)
    foto_url = graphene.String()
    aktif = graphene.Boolean(required=True, source='aktif')
    tahun_masuk = graphene.Int(required=True, source='tahunmasuk')
    kelamin = graphene.Field(Kelamin)
    tempat_lahir = graphene.String(source='tmplahir')
    tanggal_lahir = graphene.Date(source='tgllahir')
    anak_ke = graphene.Int(source='anakke')
    jumlah_saudara = graphene.Int(source='jsaudara')
    status_anak = graphene.String(source='statusanak')
    jumlah_saudara_kandung = graphene.Int(source='jkandung')
    jumlah_saudara_tiri = graphene.Int(source='jtiri')
    alamat_siswa = graphene.String(source='alamatsiswa')
    kode_pos_siswa = graphene.String(source='kodepossiswa')
    telpon_siswa = graphene.String(source='telponsiswa')
    hp_siswa = graphene.String(source='hpsiswa')
    email_siswa = graphene.String(source='emailsiswa')
    asal_sekolah = graphene.Field(AsalSekolahNode, source='asalsekolah')
    nomor_ijasah = graphene.String(source='noijasah')
    tanggal_ijasah = graphene.String(source='tglijasah')
    keterangan_sekolah = graphene.String(source='ketsekolah')
    nama_ayah = graphene.String(source='namaayah')
    nama_ibu = graphene.String(source='namaibu')
    status_ayah = graphene.String(source='statusayah')
    status_ibu = graphene.String(source='statusibu')
    tempat_lahir_ayah = graphene.String(source='tmplahirayah')
    tempat_lahir_ibu = graphene.String(source='tmplahiribu')
    tanggal_lahir_ayah = graphene.String(source='tgllahirayah')
    tanggal_lahir_ibu = graphene.String(source='tgllahiribu')
    almarhum_ayah = graphene.Boolean(source='almayah')
    almarhum_ibu = graphene.Boolean(source='almibu')
    pendidikan_ayah = graphene.Field(TingkatPendidikanNode, source='pendidikanayah')
    pendidikan_ibu = graphene.Field(TingkatPendidikanNode, source='pendidikanibu')
    pekerjaan_ayah = graphene.Field(JenisPekerjaanNode, source='pekerjaanayah')
    pekerjaan_ibu = graphene.Field(JenisPekerjaanNode, source='pekerjaanibu')
    penghasilan_ayah = graphene.Int(source='penghasilanayah')
    penghasilan_ibu = graphene.Int(source='penghasilanibu')
    alamat_ortu = graphene.String(source='alamatortu')
    telpon_ortu = graphene.String(source='telponortu')
    hp_ortu = graphene.String(source='hportu')
    email_ayah = graphene.String(source='emailayah')
    email_ibu = graphene.String(source='emailibu')
    alamat_surat = graphene.String(source='alamatsurat')
    frompsb = graphene.Boolean(source='frompsb')
    status_mutasi = graphene.Field(JenisMutasiNode, source='statusmutasi')

    is_alumni = graphene.Boolean(source='alumni')
    created_at = graphene.DateTime(required=True, source='ts')

    def resolve_foto_url(root, info):
        rf = RiwayatFoto.objects.filter(nis=root.pk).order_by('-replid').only('replid').first()
        if rf is None:
            url = django_reverse('view_photo', kwargs={'photo_type': 'siswa', 'photo_id': root.pk})
        else:
            url = django_reverse('view_photo_riwayat', kwargs={'photo_type': 'siswa', 'photo_id': root.pk, 'riwayat_id': rf.pk})
        return url

    @classmethod
    def get_queryset(cls, queryset, info):
        """
        foto is a mediumblob that could take to 16MB
        TODO also exclude other fields when querying many
        """

        return queryset \
            .filter(alumni=0) \
            .filter(aktif=1) \
            .defer("foto")


class BagianPegawaiNode(DjangoObjectType):
    class Meta:
        model = BagianPegawai
        name = 'BagianPegawai'
        interfaces = (graphene.relay.Node,)
        fields = (
            'urutan',
            'replid',
            'info1',
            'info2',
            'info3',
            'ts',
        )

    nama = graphene.String(source='bagian', required=True)


class PegawaiFilter(django_filters.FilterSet):
    nama = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Pegawai
        fields = ['nama', 'bagian']

    order_by = OrderingFilter(
        fields=(
            'nama', 'nip',
        )
    )


class PegawaiNode(DjangoObjectType):
    class Meta:
        model = Pegawai
        name = 'Pegawai'
        exclude = (
            "token",
            "issync",
            "pinpegawai",
            "foto",
        )
        filterset_class = PegawaiFilter
        interfaces = (graphene.relay.Node,)
        connection_class = ExtendedConnection

    agama = graphene.String()
    suku = graphene.String()
    guru = graphene.Field('akademik.schema.akademik.types.GuruNode')
    foto_url = graphene.String(required=True)

    def resolve_agama(root, info):
        return root.agama.agama if root.agama else None

    def resolve_suku(root, info):
        return root.suku.suku if root.suku else None

    def resolve_guru(root, info):
        return Guru.objects.filter(nip=root.nip).first()

    def resolve_foto_url(root, info):
        return django_reverse('view_photo', kwargs={'photo_type': 'pegawai', 'photo_id': root.pk})


class KelasFilter(django_filters.FilterSet):
    class Meta:
        model = Kelas
        fields = ['idtahunajaran', 'idtingkat', 'nipwali']


class KelasNode(DjangoObjectType):
    class Meta:
        model = Kelas
        name = 'Kelas'
        fields = (
            "info1",
            'info2',
            'info3',
            'siswa_set',
        )
        filterset_class = KelasFilter
        connection_class = ExtendedConnection
        interfaces = (graphene.relay.Node,)

    wali = graphene.Field(PegawaiNode, source='nipwali', required=True)
    nama = graphene.String(source='kelas', required=True)
    tingkat = graphene.Field(TingkatNode, required=True, source='idtingkat')
    tahun_ajaran = graphene.Field('akademik.schema.akademik.types.TahunAjaranNode',
                                  source='idtahunajaran', required=True)
    kapasitas = graphene.Int(required=True)
    created_at = graphene.DateTime(required=True, source='ts')


class TahunAjaranFilter(django_filters.FilterSet):
    class Meta:
        model = TahunAjaran
        fields = ['departemen']


class TahunAjaranNode(DjangoObjectType):
    class Meta:
        model = TahunAjaran
        name = 'TahunAjaran'
        fields = (
            'replid',
            'keterangan',
            'info1',
            'info2',
            'info3',
        )
        filterset_class = TahunAjaranFilter
        interfaces = (graphene.relay.Node,)

    nama = graphene.String(source='tahunajaran', required=True)
    departemen = graphene.Field('akademik.schema.akademik.types.DepartemenNode', required=True)
    tanggal_mulai = graphene.Date(required=True, source='tglmulai')
    tanggal_akhir = graphene.Date(required=True, source='tglakhir')
    aktif = graphene.Boolean(required=True)
    created_at = graphene.DateTime(required=True, source='ts')



class IdentitasType(DjangoObjectType):
    class Meta:
        model = Identitas
        name = 'Identitas'
        fields = (
            'nama',
            'situs',
            'email',
            'alamat1',
            'alamat2',
            'alamat3',
            'alamat4',
            'telp1',
            'telp2',
            'telp3',
            'telp4',
            'fax1',
            'fax2',
            'keterangan',
            'status',
            'perpustakaan',
            'info1',
            'info2',
            'info3',
        )

    foto_url = graphene.String(required=True)

    def resolve_foto_url(root, info):
        return django_reverse('view_photo', kwargs={'photo_type': 'identitas', 'photo_id': root.pk})


class DepartemenFilter(django_filters.FilterSet):
    class Meta:
        model = Departemen
        fields = ['nipkepsek']


class DepartemenNode(DjangoObjectType):
    class Meta:
        model = Departemen
        name = 'Departemen'
        fields = (
            'replid',
            'urutan',
            'keterangan',
            'aktif',
            'info1',
            'info2',
            'info3',
        )
        interfaces = (graphene.relay.Node,)
        filterset_class = DepartemenFilter

    nama = graphene.String(source='departemen', required=True)
    kepsek = graphene.Field(PegawaiNode, source='nipkepsek', required=True)
    identitas = graphene.Field(IdentitasType)
    created_at = graphene.DateTime(source='ts', required=True)

    def resolve_identitas(self, info):
        return Identitas.objects.get(departemen=self.departemen)


class JamType(DjangoObjectType):
    class Meta:
        model = Jam
        name = 'Jam'
        fields = (
            'replid',
            'jamke',
            'departemen',
            'jam1',
            'jam2',
            'info1',
            'info2',
            'info3',
            'ts',
        )


class PelajaranType(DjangoObjectType):
    class Meta:
        model = Pelajaran
        name = 'Pelajaran'
        fields = (
            'replid',
            'kode',
            'nama',
            'departemen',
            'idkelompok',
            'sifat',
            'aktif',
            'keterangan',
            'info1',
            'info2',
            'info3',
            'ts',
        )
        interfaces = (graphene.relay.Node,)


class InfoJadwalType(DjangoObjectType):
    class Meta:
        model = InfoJadwal
        name = 'InfoJadwal'
        fields = (
            # 'replid',
            'deskripsi',
            'aktif',
            'terlihat',
            'keterangan',
            # 'idtahunajaran',
            'info1',
            'info2',
            'info3',
            'ts',
        )

    id = graphene.Int(required=True)
    tahunajaran = graphene.Field(TahunAjaranNode, required=True)

    def resolve_id(self, info):
        return self.replid

    def resolve_tahunajaran(self, info):
        return self.idtahunajaran


class Jadwal(DjangoObjectType):
    class Meta:
        model = Jadwal
        name = 'Jadwal'
        fields = (
            # 'replid',
            # 'idkelas',
            # 'nipguru',
            # 'idpelajaran',
            'departemen',
            'infojadwal',
            'hari',
            'jamke',
            'njam',
            'sifat',
            'status',
            'keterangan',
            # 'jam1',
            # 'jam2',
            # 'idjam1',
            # 'idjam2',
            'info1',
            'info2',
            'info3',
            'ts',
        )

    id = graphene.Int(required=True)
    kelas = graphene.Field(KelasNode, required=True)
    guru = graphene.Field(PegawaiNode, required=True)
    jam1 = graphene.Field(JamType, required=True)
    jam2 = graphene.Field(JamType, required=True)
    pelajaran = graphene.Field(PelajaranType, required=True)

    def resolve_id(self, info):
        return self.replid

    def resolve_kelas(self, info):
        return self.idkelas

    def resolve_guru(self, info):
        return self.nipguru

    def resolve_jam1(self, info):
        return self.idjam1

    def resolve_jam2(self, info):
        return self.idjam2


class StatusGuruNode(DjangoObjectType):
    class Meta:
        model = StatusGuru
        name = 'StatusGuru'
        fields = (
            'replid',
            'status',
            'keterangan',
            'info1',
            'info2',
            'info3',
            'ts'
        )
        interfaces = (graphene.relay.Node,)


class GuruFilter(django_filters.FilterSet):
    class Meta:
        model = Guru
        fields = (
            'idpelajaran',
        )


class GuruNode(DjangoObjectType):
    class Meta:
        model = Guru
        name = 'Guru'
        fields = (
            'replid',
            'aktif',
            'keterangan',
            'info1',
            'info2',
            'info3',
            'ts',
        )
        filterset_class = GuruFilter
        interfaces = (graphene.relay.Node,)

    pelajaran = graphene.Field(PelajaranType, source='idpelajaran')
    status = graphene.Field(StatusGuruNode, source='statusguru')
