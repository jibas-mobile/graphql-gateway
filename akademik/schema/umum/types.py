from graphene_django import DjangoObjectType

from jibasdb.jbsumum.models import Agama, Suku


class AgamaType(DjangoObjectType):
    class Meta:
        model = Agama
        exclude = ("token", "issync",)


class SukuType(DjangoObjectType):
    class Meta:
        model = Suku
        exclude = ("token", "issync",)