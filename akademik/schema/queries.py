from graphene import relay
from graphene_django.filter.fields import DjangoFilterConnectionField

from akademik.schema.akademik.types import *


class TahunAjaranConnection(relay.Connection):
    class Meta:
        node = TahunAjaranNode


class Query(graphene.ObjectType):
    node = relay.Node.Field()

    kelas = relay.Node.Field(KelasNode)
    kelas_set = DjangoFilterConnectionField(KelasNode)

    departemen = relay.Node.Field(DepartemenNode)
    departemen_set = DjangoFilterConnectionField(DepartemenNode)

    tahun_ajaran = relay.Node.Field(TahunAjaranNode)
    tahun_ajaran_set = DjangoFilterConnectionField(TahunAjaranNode)

    pegawai = relay.Node.Field(PegawaiNode)
    pegawai_set = DjangoFilterConnectionField(PegawaiNode)

    siswa = relay.Node.Field(SiswaNode)
    siswa_set = DjangoFilterConnectionField(SiswaNode)
