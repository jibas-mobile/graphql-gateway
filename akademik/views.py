import pendulum
from django.db import  transaction
import magic
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import views
from rest_framework.exceptions import NotFound, APIException
from rest_framework.parsers import FileUploadParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from jibasdb.jbsakad.models import Siswa, RiwayatFoto


MAX_PHOTO_SIZE = 1024 * 1024


class PhotoView(views.APIView):
    parser_classes = [FileUploadParser]
    permission_classes = [IsAuthenticated]

    @method_decorator(cache_page(60 * 15))
    def get(self, request, photo_type, photo_id, riwayat_id=None):
        if photo_type == 'siswa':
            if riwayat_id is not None:
                rf = RiwayatFoto.objects.filter(pk=riwayat_id, nis=photo_id).only('foto').first()
                if rf is None or rf.foto == b'':
                    raise NotFound(detail='siswa tidak punya foto')
                foto = rf.foto
            else:
                siswa = Siswa.objects.only('foto').get(nis=photo_id)
                if siswa.foto is None or siswa.foto == b'':
                    raise NotFound(detail='siswa tidak punya foto')
                foto = siswa.foto
            mt = magic.from_buffer(foto, mime=True)
            # TODO add header cache-control etc
            return HttpResponse(foto, content_type=mt)

    def post(self, request, photo_type, photo_id):
        file_obj: InMemoryUploadedFile = request.data['file']
        if photo_type == 'siswa':
            foto = file_obj.read()
            if len(foto) > MAX_PHOTO_SIZE:
                raise APIException(detail='file too big', code=400)
            with transaction.atomic():
                siswa = Siswa.objects.only('nis').get(nis=photo_id)
                RiwayatFoto.objects.create(
                    nis=siswa,
                    foto=foto,
                    tanggal=pendulum.now()
                )
                Siswa.objects.filter(nis=photo_id).update(foto=foto)
            return Response(status=204)
