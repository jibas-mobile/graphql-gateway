from datetime import date

from jibasdb.jbsakad.models import PresensiHarian


def get_presensi_date(id_kelas: int, id_semester: int, dt: date):
    """
    SELECT tanggal1, tanggal2 FROM presensiharian WHERE
    (
        (
            (
                tanggal1 BETWEEN '2013-06-02' AND '2013-06-02'
            ) OR
            (
                tanggal2 BETWEEN '2013-06-02' AND '2013-06-02'
            )
        ) OR
        (
            (
                '2013-06-02' BETWEEN tanggal1 AND tanggal2
            ) OR
            (
                '2013-06-02' BETWEEN tanggal1 AND tanggal2
            )
        )
    ) AND idkelas = '46' AND idsemester = '20'
    """

    return PresensiHarian.objects. \
        filter(
        idkelas=id_kelas, idsemester=id_semester,
        tanggal1__gte=dt, tanggal2__lte=dt
    ).first()
