from django.conf import settings


JIBAS_DATABASES = settings.JIBAS_DATABASES
JIBAS_TABLE_MANAGED = settings.JIBAS_TABLE_MANAGED


class JibasDBRouter:
    def db_for_read(self, model, **hints):
        if JIBAS_TABLE_MANAGED:
            return 'default'
        if model._meta.app_label in JIBAS_DATABASES:
            return model._meta.app_label
        return None

    def db_for_write(self, model, **hints):
        if JIBAS_TABLE_MANAGED:
            return 'default'
        if model._meta.app_label in JIBAS_DATABASES:
            return model._meta.app_label
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if JIBAS_TABLE_MANAGED:
            return True
        if obj1._meta.app_label in JIBAS_DATABASES and obj2._meta.app_label in JIBAS_DATABASES:
            return True
            # No opinion if neither object is in the Example app (defer to default or other routers).
        elif obj1._meta.app_label in JIBAS_DATABASES or obj2._meta.app_label in JIBAS_DATABASES:
            return False
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if JIBAS_TABLE_MANAGED:
            return True

        if app_label in JIBAS_DATABASES:
            is_same = app_label == db
            if is_same:
                pass
            return app_label == db
        elif db in JIBAS_DATABASES:
            return False

        return None
