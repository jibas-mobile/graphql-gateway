from .base import *

DEBUG = True
JIBAS_TABLE_MANAGED = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    }
}

