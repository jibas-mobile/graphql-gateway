import rest_framework
from graphql_extensions.views import GraphQLView as BaseGraphQLView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from time import sleep


class DRFAuthenticatedGraphQLView(BaseGraphQLView):
    def parse_body(self, request):
        # sleep(3.0)
        if isinstance(request, rest_framework.request.Request):
            return request.data
        return super(BaseGraphQLView, self).parse_body(request)

    @classmethod
    def as_view(cls, *args, **kwargs):
        view = super(BaseGraphQLView, cls).as_view(*args, **kwargs)
        # Authentication will be checked in each resolver
        view = permission_classes((IsAuthenticated,))(view)
        # view = authentication_classes((JimiTokenAuthentication,))(view)
        view = api_view(['GET', 'POST'])(view)
        return view
