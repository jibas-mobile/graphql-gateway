import graphene

import akademik.schema.queries
import jimiauth.schema
import akademik.schema


class Query(jimiauth.schema.Query,
            akademik.schema.queries.Query,
            graphene.ObjectType):
    pass


class Mutation(jimiauth.schema.Mutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
