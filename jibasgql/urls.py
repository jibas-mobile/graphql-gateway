from django.urls import path
from .views import DRFAuthenticatedGraphQLView

from akademik.views import PhotoView
from jimiauth.views import pegawai_auth


urlpatterns = [
    # We don't need admin for now
    # path('admin/', admin.site.urls),
    path('api/auth/pegawai', pegawai_auth),
    # path('api/photo/<str:photo_type>/<str:photo_id>', view_photo, name='view_photo'),
    path('api/photo/<str:photo_type>/<str:photo_id>', PhotoView.as_view(), name='view_photo'),
    path('api/photo/<str:photo_type>/<str:photo_id>/<int:riwayat_id>', PhotoView.as_view(), name='view_photo_riwayat'),
    path('graphql', DRFAuthenticatedGraphQLView.as_view(graphiql=True)),
]
