from graphene_django import DjangoObjectType


class AuthObjectType(DjangoObjectType):
    class Meta:
        abstract = True

    @classmethod
    def get_queryset(cls, queryset, info):
        if info.context.user.is_authenticated():
            return super(AuthObjectType, cls).get_queryset(queryset, info)
        return cls._meta.model.objects.none()

    @classmethod
    def get_node(cls, info, id):
        if info.context.user.is_authenticated():
            return super(AuthObjectType, cls).get_node(info, id)
        return cls._meta.model.objects.none()