from django.conf import settings
from django.db import models


class JibasAkadModel(models.Model):
    class Meta:
        abstract = True

    class JibasMeta:
        db_name = 'jbsakad'


class AktivitasKalender(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idkalender = models.ForeignKey('KalenderAkademik', models.DO_NOTHING, db_column='idkalender')
    tanggalawal = models.DateField()
    tanggalakhir = models.DateField()
    kegiatan = models.CharField(max_length=50)
    keterangan = models.TextField(blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'aktivitaskalender'


class Alumni(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis', related_name='+')
    klsakhir = models.ForeignKey('Kelas', models.DO_NOTHING, db_column='klsakhir')
    tktakhir = models.ForeignKey('Tingkat', models.DO_NOTHING, db_column='tktakhir')
    tgllulus = models.DateField(blank=True, null=True)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    departemen = models.ForeignKey('Departemen', models.DO_NOTHING, db_column='departemen')
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'alumni'


class Angkatan(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    angkatan = models.CharField(max_length=50)
    departemen = models.ForeignKey('Departemen', models.DO_NOTHING, db_column='departemen')
    aktif = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'angkatan'

    class JibasMeta:
        db_name = 'jbsakad'


class AsalSekolah(JibasAkadModel):
    replid = models.PositiveIntegerField(unique=True)
    departemen = models.CharField(max_length=50)
    sekolah = models.CharField(primary_key=True, max_length=100)
    urutan = models.PositiveIntegerField(blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'asalsekolah'

    class JibasMeta:
        db_name = 'jbsakad'


class AturanGrading(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nipguru = models.ForeignKey('jbssdm.Pegawai', models.DO_NOTHING, db_column='nipguru')
    idtingkat = models.ForeignKey('Tingkat', models.DO_NOTHING, db_column='idtingkat')
    idpelajaran = models.ForeignKey('Pelajaran', models.DO_NOTHING, db_column='idpelajaran')
    dasarpenilaian = models.ForeignKey('DasarPenilaian', models.DO_NOTHING, db_column='dasarpenilaian')
    nmin = models.DecimalField(max_digits=6, decimal_places=1)
    nmax = models.DecimalField(max_digits=6, decimal_places=1)
    grade = models.CharField(max_length=2)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'aturangrading'


class Aturannhb(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nipguru = models.ForeignKey('jbssdm.Pegawai', models.DO_NOTHING, db_column='nipguru')
    idtingkat = models.ForeignKey('Tingkat', models.DO_NOTHING, db_column='idtingkat')
    idpelajaran = models.ForeignKey('Pelajaran', models.DO_NOTHING, db_column='idpelajaran')
    dasarpenilaian = models.ForeignKey('DasarPenilaian', models.DO_NOTHING, db_column='dasarpenilaian')
    idjenisujian = models.ForeignKey('JenisUjian', models.DO_NOTHING, db_column='idjenisujian')
    bobot = models.PositiveIntegerField()
    aktif = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'aturannhb'


class Auditnilai(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    jenisnilai = models.CharField(max_length=45)
    idnilai = models.PositiveIntegerField()
    nasli = models.DecimalField(max_digits=10, decimal_places=2)
    nubah = models.DecimalField(max_digits=10, decimal_places=2)
    tanggal = models.DateTimeField()
    alasan = models.CharField(max_length=255)
    pengguna = models.CharField(max_length=100)
    informasi = models.CharField(max_length=255)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'auditnilai'


class Bobotnau(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idinfo = models.ForeignKey('Infobobotnau', models.DO_NOTHING, db_column='idinfo', blank=True, null=True)
    idujian = models.ForeignKey('Ujian', models.DO_NOTHING, db_column='idujian')
    bobot = models.PositiveIntegerField()
    idaturan = models.PositiveIntegerField(blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'bobotnau'


class CalonSiswa(JibasAkadModel):
    replid = models.PositiveIntegerField(unique=True)
    nopendaftaran = models.CharField(primary_key=True, max_length=20)
    nisn = models.CharField(max_length=50, blank=True, null=True)
    nik = models.CharField(max_length=50, blank=True, null=True)
    noun = models.CharField(max_length=50, blank=True, null=True)
    nama = models.CharField(max_length=100)
    panggilan = models.CharField(max_length=30, blank=True, null=True)
    aktif = models.PositiveIntegerField()
    tahunmasuk = models.PositiveIntegerField()
    idproses = models.ForeignKey('ProsesPenerimaanSiswa', models.DO_NOTHING, db_column='idproses')
    idkelompok = models.ForeignKey('KelompokCalonSiswa', models.DO_NOTHING, db_column='idkelompok')
    suku = models.ForeignKey('jbsumum.Suku', models.DO_NOTHING, db_column='suku', blank=True, null=True)
    agama = models.ForeignKey('jbsumum.Agama', models.DO_NOTHING, db_column='agama', blank=True, null=True)
    status = models.ForeignKey('StatusSiswa', models.DO_NOTHING, db_column='status', blank=True, null=True)
    kondisi = models.ForeignKey('KondisiSiswa', models.DO_NOTHING, db_column='kondisi', blank=True, null=True)
    kelamin = models.CharField(max_length=1, blank=True, null=True)
    tmplahir = models.CharField(max_length=50, blank=True, null=True)
    tgllahir = models.DateField(blank=True, null=True)
    warga = models.CharField(max_length=5, blank=True, null=True)
    anakke = models.PositiveIntegerField(blank=True, null=True)
    jsaudara = models.PositiveIntegerField(blank=True, null=True)
    statusanak = models.CharField(max_length=10, blank=True, null=True)
    jkandung = models.PositiveIntegerField(blank=True, null=True)
    jtiri = models.PositiveIntegerField(blank=True, null=True)
    bahasa = models.CharField(max_length=30, blank=True, null=True)
    berat = models.DecimalField(max_digits=4, decimal_places=1, blank=True, null=True)
    tinggi = models.DecimalField(max_digits=4, decimal_places=1, blank=True, null=True)
    darah = models.CharField(max_length=2, blank=True, null=True)
    foto = models.BinaryField(blank=True, null=True)
    pinsiswa = models.CharField(max_length=25)
    alamatsiswa = models.CharField(max_length=255, blank=True, null=True)
    jarak = models.PositiveIntegerField(blank=True, null=True)
    kodepossiswa = models.CharField(max_length=8, blank=True, null=True)
    telponsiswa = models.CharField(max_length=20, blank=True, null=True)
    hpsiswa = models.CharField(max_length=20, blank=True, null=True)
    emailsiswa = models.CharField(max_length=100, blank=True, null=True)
    kesehatan = models.CharField(max_length=150, blank=True, null=True)
    asalsekolah = models.ForeignKey(AsalSekolah, models.DO_NOTHING, db_column='asalsekolah', blank=True, null=True)
    noijasah = models.CharField(max_length=25, blank=True, null=True)
    tglijasah = models.CharField(max_length=25, blank=True, null=True)
    ketsekolah = models.CharField(max_length=100, blank=True, null=True)
    namaayah = models.CharField(max_length=60, blank=True, null=True)
    namaibu = models.CharField(max_length=60, blank=True, null=True)
    statusayah = models.CharField(max_length=10, blank=True, null=True)
    statusibu = models.CharField(max_length=10, blank=True, null=True)
    tmplahirayah = models.CharField(max_length=35, blank=True, null=True)
    tmplahiribu = models.CharField(max_length=35, blank=True, null=True)
    tgllahirayah = models.CharField(max_length=35, blank=True, null=True)
    tgllahiribu = models.CharField(max_length=35, blank=True, null=True)
    almayah = models.PositiveIntegerField()
    almibu = models.PositiveIntegerField()
    pendidikanayah = models.ForeignKey('jbsumum.TingkatPendidikan', models.DO_NOTHING, db_column='pendidikanayah', blank=True, null=True, related_name='+')
    pendidikanibu = models.ForeignKey('jbsumum.TingkatPendidikan', models.DO_NOTHING, db_column='pendidikanibu', blank=True, null=True, related_name='+')
    pekerjaanayah = models.ForeignKey('jbsumum.JenisPekerjaan', models.DO_NOTHING, db_column='pekerjaanayah', blank=True, null=True, related_name='+')
    pekerjaanibu = models.ForeignKey('jbsumum.JenisPekerjaan', models.DO_NOTHING, db_column='pekerjaanibu', blank=True, null=True, related_name='+')
    wali = models.CharField(max_length=60, blank=True, null=True)
    penghasilanayah = models.PositiveIntegerField(blank=True, null=True)
    penghasilanibu = models.PositiveIntegerField(blank=True, null=True)
    alamatortu = models.CharField(max_length=100, blank=True, null=True)
    telponortu = models.CharField(max_length=20, blank=True, null=True)
    hportu = models.CharField(max_length=20, blank=True, null=True)
    emailayah = models.CharField(max_length=100, blank=True, null=True)
    alamatsurat = models.CharField(max_length=100, blank=True, null=True)
    keterangan = models.TextField(blank=True, null=True)
    hobi = models.TextField(blank=True, null=True)
    replidsiswa = models.PositiveIntegerField(blank=True, null=True)
    emailibu = models.CharField(max_length=100, blank=True, null=True)
    sum1 = models.DecimalField(max_digits=10, decimal_places=0)
    sum2 = models.DecimalField(max_digits=10, decimal_places=0)
    ujian1 = models.DecimalField(max_digits=5, decimal_places=2)
    ujian2 = models.DecimalField(max_digits=5, decimal_places=2)
    ujian3 = models.DecimalField(max_digits=5, decimal_places=2)
    ujian4 = models.DecimalField(max_digits=5, decimal_places=2)
    ujian5 = models.DecimalField(max_digits=5, decimal_places=2)
    ujian6 = models.DecimalField(max_digits=5, decimal_places=2)
    ujian7 = models.DecimalField(max_digits=5, decimal_places=2)
    ujian8 = models.DecimalField(max_digits=5, decimal_places=2)
    ujian9 = models.DecimalField(max_digits=5, decimal_places=2)
    ujian10 = models.DecimalField(max_digits=5, decimal_places=2)
    info1 = models.CharField(max_length=20, blank=True, null=True)
    info2 = models.CharField(max_length=20, blank=True, null=True)
    info3 = models.CharField(max_length=20, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'calonsiswa'


class DasarPenilaian(JibasAkadModel):
    replid = models.PositiveIntegerField(unique=True)
    dasarpenilaian = models.CharField(primary_key=True, max_length=50)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'dasarpenilaian'


class DeletedData(JibasAkadModel):
    tablename = models.CharField(max_length=100)
    rowid = models.PositiveBigIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'deleteddata'


class Departemen(JibasAkadModel):
    replid = models.PositiveIntegerField(unique=True)
    departemen = models.CharField(primary_key=True, max_length=50)
    nipkepsek = models.ForeignKey('jbssdm.Pegawai', models.DO_NOTHING, db_column='nipkepsek', related_name='kepsek_set')
    urutan = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    aktif = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'departemen'


class Guru(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nip = models.OneToOneField('jbssdm.Pegawai', models.DO_NOTHING, db_column='nip', related_name='guru')
    idpelajaran = models.ForeignKey('Pelajaran', models.DO_NOTHING, db_column='idpelajaran')
    statusguru = models.ForeignKey('StatusGuru', models.DO_NOTHING, db_column='statusguru')
    aktif = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'guru'


class Infobobotnau(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idujian = models.ForeignKey('Ujian', models.DO_NOTHING, db_column='idujian')
    idaturan = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'infobobotnau'


class InfoBobotUjian(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idpelajaran = models.ForeignKey('Pelajaran', models.DO_NOTHING, db_column='idpelajaran')
    idkelas = models.ForeignKey('Kelas', models.DO_NOTHING, db_column='idkelas')
    idsemester = models.ForeignKey('Semester', models.DO_NOTHING, db_column='idsemester')
    idjenisujian = models.ForeignKey('JenisUjian', models.DO_NOTHING, db_column='idjenisujian')
    pilihan = models.IntegerField()
    info = models.CharField(max_length=100)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'infobobotujian'


class InfoJadwal(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    deskripsi = models.CharField(max_length=100)
    aktif = models.PositiveIntegerField()
    terlihat = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    idtahunajaran = models.ForeignKey('TahunAjaran', models.DO_NOTHING, db_column='idtahunajaran')
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'infojadwal'


class Infonap(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idpelajaran = models.ForeignKey('Pelajaran', models.DO_NOTHING, db_column='idpelajaran')
    idsemester = models.ForeignKey('Semester', models.DO_NOTHING, db_column='idsemester')
    idkelas = models.ForeignKey('Kelas', models.DO_NOTHING, db_column='idkelas')
    nilaimin = models.DecimalField(max_digits=5, decimal_places=2)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'infonap'


class Jadwal(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idkelas = models.ForeignKey('Kelas', models.DO_NOTHING, db_column='idkelas')
    nipguru = models.ForeignKey('jbssdm.Pegawai', models.DO_NOTHING, db_column='nipguru')
    idpelajaran = models.ForeignKey('Pelajaran', models.DO_NOTHING, db_column='idpelajaran')
    departemen = models.ForeignKey(Departemen, models.DO_NOTHING, db_column='departemen')
    infojadwal = models.ForeignKey(InfoJadwal, models.DO_NOTHING, db_column='infojadwal')
    hari = models.PositiveIntegerField()
    jamke = models.PositiveIntegerField()
    njam = models.PositiveIntegerField()
    sifat = models.PositiveIntegerField()
    status = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    jam1 = models.TimeField()
    jam2 = models.TimeField()
    idjam1 = models.ForeignKey('Jam', models.DO_NOTHING, db_column='idjam1', related_name='+')
    idjam2 = models.ForeignKey('Jam', models.DO_NOTHING, db_column='idjam2', related_name='+')
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'jadwal'


class Jam(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    jamke = models.PositiveIntegerField()
    departemen = models.ForeignKey(Departemen, models.DO_NOTHING, db_column='departemen')
    jam1 = models.TimeField()
    jam2 = models.TimeField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'jam'


class JenisMutasi(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    jenismutasi = models.CharField(max_length=45)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'jenismutasi'


class JenisUjian(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    jenisujian = models.CharField(max_length=50)
    idpelajaran = models.ForeignKey('Pelajaran', models.DO_NOTHING, db_column='idpelajaran')
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'jenisujian'


class KalenderAkademik(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    kalender = models.CharField(max_length=50)
    aktif = models.PositiveIntegerField()
    terlihat = models.PositiveIntegerField()
    idtahunajaran = models.ForeignKey('TahunAjaran', models.DO_NOTHING, db_column='idtahunajaran')
    departemen = models.ForeignKey(Departemen, models.DO_NOTHING, db_column='departemen')
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'kalenderakademik'


class KejadianPenting(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idkriteria = models.PositiveIntegerField()
    nis = models.CharField(max_length=15)
    nip = models.CharField(max_length=15)
    tanggal = models.DateField()
    kejadian = models.TextField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'kejadianpenting'


class Kelas(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    kelas = models.CharField(max_length=50)
    idtahunajaran = models.ForeignKey('TahunAjaran', models.DO_NOTHING, db_column='idtahunajaran',
                                      related_name='daftar_kelas')
    kapasitas = models.PositiveIntegerField()
    nipwali = models.ForeignKey('jbssdm.Pegawai', models.DO_NOTHING, db_column='nipwali')
    aktif = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    idtingkat = models.ForeignKey('Tingkat', models.DO_NOTHING, db_column='idtingkat')
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'kelas'


class KelompokCalonSiswa(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    kelompok = models.CharField(max_length=100)
    idproses = models.ForeignKey('ProsesPenerimaanSiswa', models.DO_NOTHING, db_column='idproses')
    kapasitas = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'kelompokcalonsiswa'


class KelompokPelajaran(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    kode = models.CharField(max_length=5)
    kelompok = models.CharField(max_length=45)
    urutan = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'kelompokpelajaran'


class KomenNAP(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis')
    idinfo = models.ForeignKey(Infonap, models.DO_NOTHING, db_column='idinfo')
    komentar = models.TextField()
    predikat = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'komennap'


class KomenRapor(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis')
    idkelas = models.ForeignKey(Kelas, models.DO_NOTHING, db_column='idkelas')
    idsemester = models.ForeignKey('Semester', models.DO_NOTHING, db_column='idsemester')
    jenis = models.CharField(max_length=3)
    komentar = models.TextField()
    predikat = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'komenrapor'


class KondisiSiswa(JibasAkadModel):
    replid = models.PositiveIntegerField(unique=True)
    kondisi = models.CharField(primary_key=True, max_length=100)
    urutan = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'kondisisiswa'


class KriteriaKejadian(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    kriteria = models.CharField(max_length=50)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'kriteriakejadian'


class MutasiSiswa(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis')
    jenismutasi = models.ForeignKey(JenisMutasi, models.DO_NOTHING, db_column='jenismutasi')
    tglmutasi = models.DateField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    departemen = models.ForeignKey(Departemen, models.DO_NOTHING, db_column='departemen')
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'mutasisiswa'


class NilaiAkhirPelajaran(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis')
    idaturan = models.ForeignKey(Aturannhb, models.DO_NOTHING, db_column='idaturan')
    idinfo = models.ForeignKey(Infonap, models.DO_NOTHING, db_column='idinfo')
    nilaiangka = models.DecimalField(max_digits=10, decimal_places=2)
    nilaihuruf = models.CharField(max_length=2)
    komentar = models.TextField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'nap'


class NilaiAkhirUjian(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idpelajaran = models.ForeignKey('Pelajaran', models.DO_NOTHING, db_column='idpelajaran')
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis')
    idkelas = models.ForeignKey(Kelas, models.DO_NOTHING, db_column='idkelas', blank=True, null=True)
    idsemester = models.ForeignKey('Semester', models.DO_NOTHING, db_column='idsemester')
    idjenis = models.ForeignKey(JenisUjian, models.DO_NOTHING, db_column='idjenis')
    nilaiau = models.DecimalField(db_column='nilaiAU', max_digits=10, decimal_places=2)  # Field name made lowercase.
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    idaturan = models.ForeignKey(Aturannhb, models.DO_NOTHING, db_column='idaturan')
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'nau'


class NilaiUjian(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idujian = models.ForeignKey('Ujian', models.DO_NOTHING, db_column='idujian')
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis')
    nilaiujian = models.DecimalField(max_digits=10, decimal_places=2)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'nilaiujian'


class Pelajaran(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    kode = models.CharField(max_length=20)
    nama = models.CharField(max_length=50)
    departemen = models.ForeignKey(Departemen, models.DO_NOTHING, db_column='departemen')
    idkelompok = models.ForeignKey(KelompokPelajaran, models.DO_NOTHING, db_column='idkelompok')
    sifat = models.PositiveIntegerField()
    aktif = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'pelajaran'


class PresensiHarianSiswa(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idpresensi = models.ForeignKey('PresensiHarian', models.DO_NOTHING, db_column='idpresensi')
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis')
    hadir = models.PositiveSmallIntegerField()
    ijin = models.PositiveSmallIntegerField()
    sakit = models.PositiveSmallIntegerField()
    cuti = models.PositiveSmallIntegerField()
    alpa = models.PositiveSmallIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'phsiswa'


class AkadPilihandata(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idtambahan = models.ForeignKey('AkadTambahanData', models.DO_NOTHING, db_column='idtambahan')
    pilihan = models.CharField(max_length=255)
    aktif = models.PositiveIntegerField()
    urutan = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'pilihandata'


class Pilihkomenpel(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idpelajaran = models.ForeignKey(Pelajaran, models.DO_NOTHING, db_column='idpelajaran')
    dasarpenilaian = models.ForeignKey(DasarPenilaian, models.DO_NOTHING, db_column='dasarpenilaian')
    idtingkat = models.ForeignKey('Tingkat', models.DO_NOTHING, db_column='idtingkat')
    komentar = models.TextField()
    aktif = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'pilihkomenpel'


class Pilihkomensos(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idpelajaran = models.ForeignKey(Pelajaran, models.DO_NOTHING, db_column='idpelajaran')
    jenis = models.CharField(max_length=3)
    idtingkat = models.ForeignKey('Tingkat', models.DO_NOTHING, db_column='idtingkat')
    komentar = models.TextField()
    aktif = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'pilihkomensos'


class PresensiPelajaranSiswa(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idpp = models.ForeignKey('PresensiPelajaran', models.DO_NOTHING, db_column='idpp')
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis')
    statushadir = models.PositiveIntegerField()
    catatan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'ppsiswa'


class Ppsiswahadir(JibasAkadModel):
    """Unused?"""
    replid = models.AutoField(primary_key=True)
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis')
    idkelas = models.ForeignKey(Kelas, models.DO_NOTHING, db_column='idkelas')
    idsemester = models.ForeignKey('Semester', models.DO_NOTHING, db_column='idsemester')
    idpelajaran = models.ForeignKey(Pelajaran, models.DO_NOTHING, db_column='idpelajaran')
    gurupelajaran = models.ForeignKey('jbssdm.Pegawai', models.DO_NOTHING, db_column='gurupelajaran')
    bulan = models.PositiveIntegerField()
    tahun = models.PositiveSmallIntegerField()
    hadir = models.PositiveSmallIntegerField()
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'ppsiswahadir'
        unique_together = (('nis', 'idkelas', 'idsemester', 'idpelajaran', 'gurupelajaran', 'bulan', 'tahun'),)


class PresensiHarian(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idkelas = models.ForeignKey(Kelas, models.DO_NOTHING, db_column='idkelas')
    idsemester = models.ForeignKey('Semester', models.DO_NOTHING, db_column='idsemester')
    tanggal1 = models.DateField()
    tanggal2 = models.DateField()
    hariaktif = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'presensiharian'


class PresensiPelajaran(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idkelas = models.ForeignKey(Kelas, models.DO_NOTHING, db_column='idkelas')
    idsemester = models.ForeignKey('Semester', models.DO_NOTHING, db_column='idsemester')
    idpelajaran = models.ForeignKey(Pelajaran, models.DO_NOTHING, db_column='idpelajaran')
    tanggal = models.DateField()
    jam = models.TimeField()
    gurupelajaran = models.ForeignKey('jbssdm.Pegawai', models.DO_NOTHING, db_column='gurupelajaran')
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    materi = models.CharField(max_length=1000)
    objektif = models.CharField(max_length=255, blank=True, null=True)
    refleksi = models.CharField(max_length=255, blank=True, null=True)
    rencana = models.CharField(max_length=255, blank=True, null=True)
    keterlambatan = models.IntegerField(blank=True, null=True)
    jumlahjam = models.DecimalField(max_digits=4, decimal_places=2)
    jenisguru = models.ForeignKey('StatusGuru', models.DO_NOTHING, db_column='jenisguru')
    info1 = models.CharField(max_length=20, blank=True, null=True)
    info2 = models.CharField(max_length=20, blank=True, null=True)
    info3 = models.CharField(max_length=20, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'presensipelajaran'


class ProsesPenerimaanSiswa(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    proses = models.CharField(max_length=100)
    kodeawalan = models.CharField(max_length=5)
    departemen = models.ForeignKey(Departemen, models.DO_NOTHING, db_column='departemen')
    aktif = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'prosespenerimaansiswa'


class RataRataUjianKelas(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idkelas = models.ForeignKey(Kelas, models.DO_NOTHING, db_column='idkelas')
    idsemester = models.ForeignKey('Semester', models.DO_NOTHING, db_column='idsemester')
    idujian = models.ForeignKey('Ujian', models.DO_NOTHING, db_column='idujian')
    nilairk = models.DecimalField(db_column='nilaiRK', max_digits=10, decimal_places=2)  # Field name made lowercase.
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'ratauk'


class RataRataUjianSiswa(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis')
    idsemester = models.ForeignKey('Semester', models.DO_NOTHING, db_column='idsemester')
    idkelas = models.ForeignKey(Kelas, models.DO_NOTHING, db_column='idkelas', blank=True, null=True)
    idjenis = models.ForeignKey(JenisUjian, models.DO_NOTHING, db_column='idjenis')
    rataus = models.DecimalField(db_column='rataUS', max_digits=10, decimal_places=2)  # Field name made lowercase.
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    idpelajaran = models.ForeignKey(Pelajaran, models.DO_NOTHING, db_column='idpelajaran')
    idaturan = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'rataus'


class RiwayatDepartemenSiswa(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis')
    departemen = models.ForeignKey(Departemen, models.DO_NOTHING, db_column='departemen')
    mulai = models.DateField()
    aktif = models.PositiveIntegerField()
    status = models.PositiveIntegerField(blank=True, null=True)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    nislama = models.CharField(max_length=20, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'riwayatdeptsiswa'


class RiwayatFoto(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis', blank=True, null=True)
    nip = models.ForeignKey('jbssdm.Pegawai', models.DO_NOTHING, db_column='nip', blank=True, null=True)
    nic = models.ForeignKey(CalonSiswa, models.DO_NOTHING, db_column='nic', blank=True, null=True)
    foto = models.BinaryField()
    tanggal = models.DateTimeField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'riwayatfoto'


class RiwayatKelasSiswa(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nis = models.ForeignKey('Siswa', models.DO_NOTHING, db_column='nis')
    idkelas = models.ForeignKey(Kelas, models.DO_NOTHING, db_column='idkelas')
    mulai = models.DateField()
    aktif = models.PositiveIntegerField()
    status = models.PositiveIntegerField(blank=True, null=True)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'riwayatkelassiswa'


class RencanaPelaksanaanPembelajaran(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idtingkat = models.ForeignKey('Tingkat', models.DO_NOTHING, db_column='idtingkat')
    idsemester = models.ForeignKey('Semester', models.DO_NOTHING, db_column='idsemester')
    idpelajaran = models.ForeignKey(Pelajaran, models.DO_NOTHING, db_column='idpelajaran')
    koderpp = models.CharField(max_length=20)
    rpp = models.CharField(max_length=255)
    deskripsi = models.TextField(blank=True, null=True)
    aktif = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'rpp'


class Semester(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    semester = models.CharField(max_length=50)
    departemen = models.ForeignKey(Departemen, models.DO_NOTHING, db_column='departemen')
    aktif = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'semester'


class SettingPSB(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idproses = models.ForeignKey(ProsesPenerimaanSiswa, models.DO_NOTHING, db_column='idproses')
    kdsum1 = models.CharField(max_length=5, blank=True, null=True)
    nmsum1 = models.CharField(max_length=100, blank=True, null=True)
    kdsum2 = models.CharField(max_length=5, blank=True, null=True)
    nmsum2 = models.CharField(max_length=100, blank=True, null=True)
    kdujian1 = models.CharField(max_length=5, blank=True, null=True)
    nmujian1 = models.CharField(max_length=100, blank=True, null=True)
    kdujian2 = models.CharField(max_length=5, blank=True, null=True)
    nmujian2 = models.CharField(max_length=100, blank=True, null=True)
    kdujian3 = models.CharField(max_length=5, blank=True, null=True)
    nmujian3 = models.CharField(max_length=100, blank=True, null=True)
    kdujian4 = models.CharField(max_length=5, blank=True, null=True)
    nmujian4 = models.CharField(max_length=100, blank=True, null=True)
    kdujian5 = models.CharField(max_length=5, blank=True, null=True)
    nmujian5 = models.CharField(max_length=100, blank=True, null=True)
    kdujian6 = models.CharField(max_length=5, blank=True, null=True)
    nmujian6 = models.CharField(max_length=100, blank=True, null=True)
    kdujian7 = models.CharField(max_length=5, blank=True, null=True)
    nmujian7 = models.CharField(max_length=100, blank=True, null=True)
    kdujian8 = models.CharField(max_length=5, blank=True, null=True)
    nmujian8 = models.CharField(max_length=100, blank=True, null=True)
    kdujian9 = models.CharField(max_length=5, blank=True, null=True)
    nmujian9 = models.CharField(max_length=100, blank=True, null=True)
    kdujian10 = models.CharField(max_length=5, blank=True, null=True)
    nmujian10 = models.CharField(max_length=100, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'settingpsb'


class Siswa(JibasAkadModel):
    replid = models.PositiveIntegerField(unique=True)
    nis = models.CharField(primary_key=True, max_length=20)
    nisn = models.CharField(max_length=50, blank=True, null=True)
    nik = models.CharField(max_length=50, blank=True, null=True)
    noun = models.CharField(max_length=50, blank=True, null=True)
    nama = models.CharField(max_length=100)
    panggilan = models.CharField(max_length=30, blank=True, null=True)
    aktif = models.PositiveIntegerField()
    tahunmasuk = models.PositiveIntegerField()
    idangkatan = models.ForeignKey(Angkatan, models.DO_NOTHING, db_column='idangkatan')
    idkelas = models.ForeignKey(Kelas, models.DO_NOTHING, db_column='idkelas', related_name='siswa_set')
    suku = models.ForeignKey('jbsumum.Suku', models.DO_NOTHING, db_column='suku', blank=True, null=True)
    agama = models.ForeignKey('jbsumum.Agama', models.DO_NOTHING, db_column='agama', blank=True, null=True)
    status = models.ForeignKey('StatusSiswa', models.DO_NOTHING, db_column='status', blank=True, null=True)
    kondisi = models.ForeignKey(KondisiSiswa, models.DO_NOTHING, db_column='kondisi', blank=True, null=True)
    kelamin = models.CharField(max_length=1, blank=True, null=True)
    tmplahir = models.CharField(max_length=50, blank=True, null=True)
    tgllahir = models.DateField(blank=True, null=True)
    warga = models.CharField(max_length=5, blank=True, null=True)
    anakke = models.PositiveIntegerField(blank=True, null=True)
    jsaudara = models.PositiveIntegerField(blank=True, null=True)
    statusanak = models.CharField(max_length=10, blank=True, null=True)
    jkandung = models.PositiveIntegerField(blank=True, null=True)
    jtiri = models.PositiveIntegerField(blank=True, null=True)
    bahasa = models.CharField(max_length=60, blank=True, null=True)
    berat = models.DecimalField(max_digits=4, decimal_places=1, blank=True, null=True)
    tinggi = models.DecimalField(max_digits=4, decimal_places=1, blank=True, null=True)
    darah = models.CharField(max_length=2, blank=True, null=True)
    foto = models.BinaryField(blank=True, null=True)
    alamatsiswa = models.CharField(max_length=255, blank=True, null=True)
    jarak = models.PositiveIntegerField(blank=True, null=True)
    kodepossiswa = models.CharField(max_length=8, blank=True, null=True)
    telponsiswa = models.CharField(max_length=20, blank=True, null=True)
    hpsiswa = models.CharField(max_length=20, blank=True, null=True)
    emailsiswa = models.CharField(max_length=100, blank=True, null=True)
    kesehatan = models.CharField(max_length=150, blank=True, null=True)
    asalsekolah = models.ForeignKey(AsalSekolah, models.DO_NOTHING, db_column='asalsekolah', blank=True, null=True)
    noijasah = models.CharField(max_length=25, blank=True, null=True)
    tglijasah = models.CharField(max_length=25, blank=True, null=True)
    ketsekolah = models.CharField(max_length=100, blank=True, null=True)
    namaayah = models.CharField(max_length=60, blank=True, null=True)
    namaibu = models.CharField(max_length=60, blank=True, null=True)
    statusayah = models.CharField(max_length=10, blank=True, null=True)
    statusibu = models.CharField(max_length=10, blank=True, null=True)
    tmplahirayah = models.CharField(max_length=35, blank=True, null=True)
    tmplahiribu = models.CharField(max_length=35, blank=True, null=True)
    tgllahirayah = models.CharField(max_length=35, blank=True, null=True)
    tgllahiribu = models.CharField(max_length=35, blank=True, null=True)
    almayah = models.PositiveIntegerField()
    almibu = models.PositiveIntegerField()
    pendidikanayah = models.ForeignKey('jbsumum.TingkatPendidikan', models.DO_NOTHING, db_column='pendidikanayah', blank=True, null=True, related_name='+')
    pendidikanibu = models.ForeignKey('jbsumum.TingkatPendidikan', models.DO_NOTHING, db_column='pendidikanibu', blank=True, null=True, related_name='+')
    pekerjaanayah = models.ForeignKey('jbsumum.JenisPekerjaan', models.DO_NOTHING, db_column='pekerjaanayah', blank=True, null=True, related_name='+')
    pekerjaanibu = models.ForeignKey('jbsumum.JenisPekerjaan', models.DO_NOTHING, db_column='pekerjaanibu', blank=True, null=True, related_name='+')
    wali = models.CharField(max_length=60, blank=True, null=True)
    penghasilanayah = models.PositiveIntegerField(blank=True, null=True)
    penghasilanibu = models.PositiveIntegerField(blank=True, null=True)
    alamatortu = models.CharField(max_length=100, blank=True, null=True)
    telponortu = models.CharField(max_length=20, blank=True, null=True)
    hportu = models.CharField(max_length=20, blank=True, null=True)
    emailayah = models.CharField(max_length=100, blank=True, null=True)
    alamatsurat = models.CharField(max_length=100, blank=True, null=True)
    keterangan = models.TextField(blank=True, null=True)
    hobi = models.TextField(blank=True, null=True)
    frompsb = models.PositiveIntegerField(blank=True, null=True)
    ketpsb = models.CharField(max_length=100, blank=True, null=True)
    statusmutasi = models.ForeignKey(JenisMutasi, models.DO_NOTHING, db_column='statusmutasi', blank=True, null=True)
    alumni = models.PositiveIntegerField()
    pinsiswa = models.CharField(max_length=25)
    pinortu = models.CharField(max_length=25)
    pinortuibu = models.CharField(max_length=25)
    emailibu = models.CharField(max_length=100, blank=True, null=True)
    info1 = models.CharField(max_length=20, blank=True, null=True)
    info2 = models.CharField(max_length=20, blank=True, null=True)
    info3 = models.CharField(max_length=20, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'siswa'


class StatusGuru(JibasAkadModel):
    replid = models.PositiveIntegerField(unique=True)
    status = models.CharField(primary_key=True, max_length=50)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'statusguru'


class StatusSiswa(JibasAkadModel):
    replid = models.PositiveIntegerField(unique=True)
    status = models.CharField(primary_key=True, max_length=100)
    urutan = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'statussiswa'


class TahunAjaran(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    tahunajaran = models.CharField(max_length=50)
    departemen = models.ForeignKey(Departemen, models.DO_NOTHING, db_column='departemen')
    tglmulai = models.DateField()
    tglakhir = models.DateField()
    aktif = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'tahunajaran'


class AkadTambahanData(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    departemen = models.ForeignKey(Departemen, models.DO_NOTHING, db_column='departemen')
    kolom = models.CharField(max_length=100)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    jenis = models.PositiveIntegerField()
    aktif = models.PositiveIntegerField()
    urutan = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'tambahandata'


class TambahanDataCalon(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nopendaftaran = models.ForeignKey(CalonSiswa, models.DO_NOTHING, db_column='nopendaftaran')
    idtambahan = models.ForeignKey(AkadTambahanData, models.DO_NOTHING, db_column='idtambahan')
    jenis = models.PositiveIntegerField()
    teks = models.CharField(max_length=1000, blank=True, null=True)
    filedata = models.TextField(blank=True, null=True)
    filename = models.CharField(max_length=255, blank=True, null=True)
    filemime = models.CharField(max_length=255, blank=True, null=True)
    filesize = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'tambahandatacalon'


class TambahanDataSiswa(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    nis = models.ForeignKey(Siswa, models.DO_NOTHING, db_column='nis')
    idtambahan = models.ForeignKey(AkadTambahanData, models.DO_NOTHING, db_column='idtambahan')
    jenis = models.PositiveIntegerField()
    teks = models.CharField(max_length=1000, blank=True, null=True)
    filedata = models.TextField(blank=True, null=True)
    filename = models.CharField(max_length=255, blank=True, null=True)
    filemime = models.CharField(max_length=255, blank=True, null=True)
    filesize = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'tambahandatasiswa'


class Tingkat(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    tingkat = models.CharField(max_length=50)
    departemen = models.ForeignKey(Departemen, models.DO_NOTHING, db_column='departemen')
    aktif = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    urutan = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'tingkat'


class Ujian(JibasAkadModel):
    replid = models.AutoField(primary_key=True)
    idpelajaran = models.ForeignKey(Pelajaran, models.DO_NOTHING, db_column='idpelajaran')
    idkelas = models.ForeignKey(Kelas, models.DO_NOTHING, db_column='idkelas')
    idsemester = models.ForeignKey(Semester, models.DO_NOTHING, db_column='idsemester')
    idjenis = models.ForeignKey(JenisUjian, models.DO_NOTHING, db_column='idjenis')
    deskripsi = models.CharField(max_length=100)
    tanggal = models.DateField()
    tglkirimsms = models.DateField(db_column='tglkirimSMS', blank=True, null=True)  # Field name made lowercase.
    idaturan = models.ForeignKey(Aturannhb, models.DO_NOTHING, db_column='idaturan')
    idrpp = models.ForeignKey(RencanaPelaksanaanPembelajaran, models.DO_NOTHING, db_column='idrpp', blank=True, null=True)
    kode = models.CharField(max_length=20, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'ujian'
