from django.conf import settings
from django.db import models


class JibasUserModel(models.Model):
    class Meta:
        abstract = True

    class JibasMeta:
        db_name = 'jbsuser'


class AdminSiswa(JibasUserModel):
    clientid = models.CharField(max_length=5, blank=True, null=True)
    region = models.CharField(max_length=5, blank=True, null=True)
    location = models.CharField(max_length=5, blank=True, null=True)
    replid = models.AutoField(primary_key=True)
    isnew = models.CharField(max_length=45)
    haschange = models.CharField(max_length=45)
    password = models.CharField(max_length=100)
    lastlogin = models.DateTimeField()
    info1 = models.CharField(max_length=50, blank=True, null=True)
    info2 = models.CharField(max_length=50, blank=True, null=True)
    info3 = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'adminsiswa'


class HakAkses(JibasUserModel):
    clientid = models.CharField(max_length=5, blank=True, null=True)
    region = models.CharField(max_length=5, blank=True, null=True)
    location = models.CharField(max_length=5, blank=True, null=True)
    replid = models.AutoField(primary_key=True)
    login = models.ForeignKey('jbsuser.Login', models.DO_NOTHING, db_column='login')
    modul = models.CharField(max_length=100)
    tingkat = models.PositiveIntegerField()
    departemen = models.ForeignKey('jbsakad.Departemen', models.DO_NOTHING, db_column='departemen', blank=True, null=True)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    theme = models.PositiveIntegerField(blank=True, null=True)
    lastlogin = models.DateTimeField(blank=True, null=True)
    aktif = models.PositiveIntegerField()
    info1 = models.CharField(max_length=50, blank=True, null=True)
    info2 = models.CharField(max_length=50, blank=True, null=True)
    info3 = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'hakakses'


class HakAksesInfoSiswa(JibasUserModel):
    clientid = models.CharField(max_length=5, blank=True, null=True)
    region = models.CharField(max_length=5, blank=True, null=True)
    location = models.CharField(max_length=5, blank=True, null=True)
    replid = models.AutoField(primary_key=True)
    nis = models.ForeignKey('jbsakad.Siswa', models.DO_NOTHING, db_column='nis')
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    theme = models.PositiveIntegerField(blank=True, null=True)
    lastlogin = models.DateTimeField(blank=True, null=True)
    info1 = models.CharField(max_length=50, blank=True, null=True)
    info2 = models.CharField(max_length=50, blank=True, null=True)
    info3 = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'hakaksesinfosiswa'


class HakAksesKeuangan(JibasUserModel):
    clientid = models.CharField(max_length=5, blank=True, null=True)
    region = models.CharField(max_length=5, blank=True, null=True)
    location = models.CharField(max_length=5, blank=True, null=True)
    replid = models.AutoField(primary_key=True)
    login = models.CharField(max_length=20)
    modul = models.CharField(max_length=100)
    tingkat = models.PositiveIntegerField()
    departemen = models.CharField(max_length=50)
    info1 = models.CharField(max_length=50, blank=True, null=True)
    info2 = models.CharField(max_length=50, blank=True, null=True)
    info3 = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'hakakseskeuangan'


class HakAksessImaka(JibasUserModel):
    clientid = models.CharField(max_length=5, blank=True, null=True)
    region = models.CharField(max_length=5, blank=True, null=True)
    location = models.CharField(max_length=5, blank=True, null=True)
    replid = models.AutoField(primary_key=True)
    login = models.ForeignKey('jbssdm.Pegawai', models.DO_NOTHING, db_column='login')
    modul = models.ForeignKey('Modul', models.DO_NOTHING, db_column='modul')
    tingkat = models.PositiveIntegerField()
    departemen = models.CharField(max_length=50)
    info1 = models.CharField(max_length=50, blank=True, null=True)
    info2 = models.CharField(max_length=50, blank=True, null=True)
    info3 = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'hakaksessimaka'


class LandLord(JibasUserModel):
    clientid = models.CharField(max_length=5, blank=True, null=True)
    region = models.CharField(max_length=5, blank=True, null=True)
    location = models.CharField(max_length=5, blank=True, null=True)
    replid = models.AutoField(primary_key=True)
    isnew = models.CharField(max_length=45)
    haschange = models.CharField(max_length=45)
    password = models.CharField(max_length=100)
    lastlogin = models.DateTimeField(blank=True, null=True)
    info1 = models.CharField(max_length=50, blank=True, null=True)
    info2 = models.CharField(max_length=50, blank=True, null=True)
    info3 = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'landlord'


class Login(JibasUserModel):
    clientid = models.CharField(max_length=5, blank=True, null=True)
    region = models.CharField(max_length=5, blank=True, null=True)
    location = models.CharField(max_length=5, blank=True, null=True)
    replid = models.AutoField(primary_key=True)
    login = models.ForeignKey('jbssdm.Pegawai', models.DO_NOTHING, db_column='login')
    password = models.CharField(max_length=100)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    aktif = models.PositiveIntegerField()
    info1 = models.CharField(max_length=50, blank=True, null=True)
    info2 = models.CharField(max_length=50, blank=True, null=True)
    info3 = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'login'


class LoginSiswa(JibasUserModel):
    clientid = models.CharField(max_length=5, blank=True, null=True)
    region = models.CharField(max_length=5, blank=True, null=True)
    location = models.CharField(max_length=5, blank=True, null=True)
    replid = models.AutoField(primary_key=True)
    login = models.ForeignKey('jbsakad.Siswa', models.DO_NOTHING, db_column='login')
    password = models.CharField(max_length=100, blank=True, null=True)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    lastlogin = models.DateTimeField(blank=True, null=True)
    aktif = models.PositiveIntegerField()
    theme = models.IntegerField()
    info1 = models.CharField(max_length=50, blank=True, null=True)
    info2 = models.CharField(max_length=50, blank=True, null=True)
    info3 = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'loginsiswa'


class Lokasi(JibasUserModel):
    clientid = models.CharField(max_length=5, blank=True, null=True)
    region = models.CharField(max_length=5, blank=True, null=True)
    location = models.CharField(max_length=5, blank=True, null=True)
    replid = models.AutoField(primary_key=True)
    isnew = models.PositiveIntegerField()
    haschange = models.PositiveIntegerField()
    lokasi = models.CharField(max_length=45)
    singkatan = models.CharField(max_length=3)
    info1 = models.CharField(max_length=50, blank=True, null=True)
    info2 = models.CharField(max_length=50, blank=True, null=True)
    info3 = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'lokasi'


class Modul(JibasUserModel):
    clientid = models.CharField(max_length=5, blank=True, null=True)
    region = models.CharField(max_length=5, blank=True, null=True)
    location = models.CharField(max_length=5, blank=True, null=True)
    isnew = models.PositiveIntegerField()
    haschange = models.PositiveIntegerField()
    modul = models.CharField(primary_key=True, max_length=100)
    info1 = models.CharField(max_length=50, blank=True, null=True)
    info2 = models.CharField(max_length=50, blank=True, null=True)
    info3 = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'modul'
