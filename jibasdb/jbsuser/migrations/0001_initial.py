# Generated by Django 4.0.3 on 2022-03-11 05:53

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('jbsakad', '0001_initial'),
        ('jbssdm', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdminSiswa',
            fields=[
                ('clientid', models.CharField(blank=True, max_length=5, null=True)),
                ('region', models.CharField(blank=True, max_length=5, null=True)),
                ('location', models.CharField(blank=True, max_length=5, null=True)),
                ('replid', models.AutoField(primary_key=True, serialize=False)),
                ('isnew', models.CharField(max_length=45)),
                ('haschange', models.CharField(max_length=45)),
                ('password', models.CharField(max_length=100)),
                ('lastlogin', models.DateTimeField()),
                ('info1', models.CharField(blank=True, max_length=50, null=True)),
                ('info2', models.CharField(blank=True, max_length=50, null=True)),
                ('info3', models.CharField(blank=True, max_length=50, null=True)),
            ],
            options={
                'db_table': '',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='HakAksesKeuangan',
            fields=[
                ('clientid', models.CharField(blank=True, max_length=5, null=True)),
                ('region', models.CharField(blank=True, max_length=5, null=True)),
                ('location', models.CharField(blank=True, max_length=5, null=True)),
                ('replid', models.AutoField(primary_key=True, serialize=False)),
                ('login', models.CharField(max_length=20)),
                ('modul', models.CharField(max_length=100)),
                ('tingkat', models.PositiveIntegerField()),
                ('departemen', models.CharField(max_length=50)),
                ('info1', models.CharField(blank=True, max_length=50, null=True)),
                ('info2', models.CharField(blank=True, max_length=50, null=True)),
                ('info3', models.CharField(blank=True, max_length=50, null=True)),
            ],
            options={
                'db_table': '',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='LandLord',
            fields=[
                ('clientid', models.CharField(blank=True, max_length=5, null=True)),
                ('region', models.CharField(blank=True, max_length=5, null=True)),
                ('location', models.CharField(blank=True, max_length=5, null=True)),
                ('replid', models.AutoField(primary_key=True, serialize=False)),
                ('isnew', models.CharField(max_length=45)),
                ('haschange', models.CharField(max_length=45)),
                ('password', models.CharField(max_length=100)),
                ('lastlogin', models.DateTimeField(blank=True, null=True)),
                ('info1', models.CharField(blank=True, max_length=50, null=True)),
                ('info2', models.CharField(blank=True, max_length=50, null=True)),
                ('info3', models.CharField(blank=True, max_length=50, null=True)),
            ],
            options={
                'db_table': '',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Lokasi',
            fields=[
                ('clientid', models.CharField(blank=True, max_length=5, null=True)),
                ('region', models.CharField(blank=True, max_length=5, null=True)),
                ('location', models.CharField(blank=True, max_length=5, null=True)),
                ('replid', models.AutoField(primary_key=True, serialize=False)),
                ('isnew', models.PositiveIntegerField()),
                ('haschange', models.PositiveIntegerField()),
                ('lokasi', models.CharField(max_length=45)),
                ('singkatan', models.CharField(max_length=3)),
                ('info1', models.CharField(blank=True, max_length=50, null=True)),
                ('info2', models.CharField(blank=True, max_length=50, null=True)),
                ('info3', models.CharField(blank=True, max_length=50, null=True)),
            ],
            options={
                'db_table': '',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Modul',
            fields=[
                ('clientid', models.CharField(blank=True, max_length=5, null=True)),
                ('region', models.CharField(blank=True, max_length=5, null=True)),
                ('location', models.CharField(blank=True, max_length=5, null=True)),
                ('isnew', models.PositiveIntegerField()),
                ('haschange', models.PositiveIntegerField()),
                ('modul', models.CharField(max_length=100, primary_key=True, serialize=False)),
                ('info1', models.CharField(blank=True, max_length=50, null=True)),
                ('info2', models.CharField(blank=True, max_length=50, null=True)),
                ('info3', models.CharField(blank=True, max_length=50, null=True)),
            ],
            options={
                'db_table': '',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='LoginSiswa',
            fields=[
                ('clientid', models.CharField(blank=True, max_length=5, null=True)),
                ('region', models.CharField(blank=True, max_length=5, null=True)),
                ('location', models.CharField(blank=True, max_length=5, null=True)),
                ('replid', models.AutoField(primary_key=True, serialize=False)),
                ('password', models.CharField(blank=True, max_length=100, null=True)),
                ('keterangan', models.CharField(blank=True, max_length=255, null=True)),
                ('lastlogin', models.DateTimeField(blank=True, null=True)),
                ('aktif', models.PositiveIntegerField()),
                ('theme', models.IntegerField()),
                ('info1', models.CharField(blank=True, max_length=50, null=True)),
                ('info2', models.CharField(blank=True, max_length=50, null=True)),
                ('info3', models.CharField(blank=True, max_length=50, null=True)),
                ('login', models.ForeignKey(db_column='login', on_delete=django.db.models.deletion.DO_NOTHING, to='jbsakad.siswa')),
            ],
            options={
                'db_table': '',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Login',
            fields=[
                ('clientid', models.CharField(blank=True, max_length=5, null=True)),
                ('region', models.CharField(blank=True, max_length=5, null=True)),
                ('location', models.CharField(blank=True, max_length=5, null=True)),
                ('replid', models.AutoField(primary_key=True, serialize=False)),
                ('password', models.CharField(max_length=100)),
                ('keterangan', models.CharField(blank=True, max_length=255, null=True)),
                ('aktif', models.PositiveIntegerField()),
                ('info1', models.CharField(blank=True, max_length=50, null=True)),
                ('info2', models.CharField(blank=True, max_length=50, null=True)),
                ('info3', models.CharField(blank=True, max_length=50, null=True)),
                ('login', models.ForeignKey(db_column='login', on_delete=django.db.models.deletion.DO_NOTHING, to='jbssdm.pegawai')),
            ],
            options={
                'db_table': '',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='HakAksessImaka',
            fields=[
                ('clientid', models.CharField(blank=True, max_length=5, null=True)),
                ('region', models.CharField(blank=True, max_length=5, null=True)),
                ('location', models.CharField(blank=True, max_length=5, null=True)),
                ('replid', models.AutoField(primary_key=True, serialize=False)),
                ('tingkat', models.PositiveIntegerField()),
                ('departemen', models.CharField(max_length=50)),
                ('info1', models.CharField(blank=True, max_length=50, null=True)),
                ('info2', models.CharField(blank=True, max_length=50, null=True)),
                ('info3', models.CharField(blank=True, max_length=50, null=True)),
                ('login', models.ForeignKey(db_column='login', on_delete=django.db.models.deletion.DO_NOTHING, to='jbssdm.pegawai')),
                ('modul', models.ForeignKey(db_column='modul', on_delete=django.db.models.deletion.DO_NOTHING, to='jbsuser.modul')),
            ],
            options={
                'db_table': '',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='HakAksesInfoSiswa',
            fields=[
                ('clientid', models.CharField(blank=True, max_length=5, null=True)),
                ('region', models.CharField(blank=True, max_length=5, null=True)),
                ('location', models.CharField(blank=True, max_length=5, null=True)),
                ('replid', models.AutoField(primary_key=True, serialize=False)),
                ('keterangan', models.CharField(blank=True, max_length=255, null=True)),
                ('theme', models.PositiveIntegerField(blank=True, null=True)),
                ('lastlogin', models.DateTimeField(blank=True, null=True)),
                ('info1', models.CharField(blank=True, max_length=50, null=True)),
                ('info2', models.CharField(blank=True, max_length=50, null=True)),
                ('info3', models.CharField(blank=True, max_length=50, null=True)),
                ('nis', models.ForeignKey(db_column='nis', on_delete=django.db.models.deletion.DO_NOTHING, to='jbsakad.siswa')),
            ],
            options={
                'db_table': '',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='HakAkses',
            fields=[
                ('clientid', models.CharField(blank=True, max_length=5, null=True)),
                ('region', models.CharField(blank=True, max_length=5, null=True)),
                ('location', models.CharField(blank=True, max_length=5, null=True)),
                ('replid', models.AutoField(primary_key=True, serialize=False)),
                ('modul', models.CharField(max_length=100)),
                ('tingkat', models.PositiveIntegerField()),
                ('keterangan', models.CharField(blank=True, max_length=255, null=True)),
                ('theme', models.PositiveIntegerField(blank=True, null=True)),
                ('lastlogin', models.DateTimeField(blank=True, null=True)),
                ('aktif', models.PositiveIntegerField()),
                ('info1', models.CharField(blank=True, max_length=50, null=True)),
                ('info2', models.CharField(blank=True, max_length=50, null=True)),
                ('info3', models.CharField(blank=True, max_length=50, null=True)),
                ('departemen', models.ForeignKey(blank=True, db_column='departemen', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='jbsakad.departemen')),
                ('login', models.ForeignKey(db_column='login', on_delete=django.db.models.deletion.DO_NOTHING, to='jbsuser.login')),
            ],
            options={
                'db_table': '',
                'managed': True,
            },
        ),
    ] if settings.JIBAS_TABLE_MANAGED else []
