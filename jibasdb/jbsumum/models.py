from django.conf import settings
from django.db import models


class JibasUmumModel(models.Model):
    class Meta:
        abstract = True

    class JibasMeta:
        db_name = 'jbsumum'


class Agama(JibasUmumModel):
    replid = models.PositiveIntegerField(unique=True)
    agama = models.CharField(primary_key=True, max_length=20)
    urutan = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'agama'


class Appregis(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    transid = models.CharField(max_length=45)
    transkey = models.CharField(max_length=45)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'appregis'


class Appupload(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    app = models.CharField(max_length=45)
    upldate = models.DateTimeField()
    batchid = models.PositiveIntegerField()
    filename = models.CharField(max_length=255)
    filepath = models.CharField(max_length=255)
    filetype = models.CharField(max_length=45)
    filesize = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'appupload'


class Card(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    departemen = models.ForeignKey('jbsakad.Departemen', models.DO_NOTHING, db_column='departemen')
    judul = models.CharField(max_length=255)
    aktif = models.IntegerField()
    latar = models.TextField()
    latardata = models.TextField(blank=True, null=True)
    tanggal = models.DateTimeField()
    deskripsi = models.CharField(max_length=255, blank=True, null=True)
    vres = models.DecimalField(max_digits=5, decimal_places=2)
    hres = models.DecimalField(max_digits=5, decimal_places=2)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'card'


class Cardaktifitas(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    departemen = models.ForeignKey('jbsakad.Departemen', models.DO_NOTHING, db_column='departemen')
    judul = models.CharField(max_length=100)
    siswa = models.PositiveIntegerField()
    pegawai = models.PositiveIntegerField()
    calonsiswa = models.PositiveIntegerField()
    validasipin = models.PositiveIntegerField()
    inputketerangan = models.PositiveIntegerField()
    pengulangan = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    aktif = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'cardaktifitas'


class Carddata(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    idcard = models.ForeignKey(Card, models.DO_NOTHING, db_column='idcard')
    carddatatype = models.PositiveIntegerField()
    text = models.CharField(max_length=2000, blank=True, null=True)
    fontname = models.CharField(max_length=255, blank=True, null=True)
    fontsize = models.CharField(max_length=255, blank=True, null=True)
    fontstyle = models.CharField(max_length=255, blank=True, null=True)
    color = models.CharField(max_length=12, blank=True, null=True)
    posx = models.PositiveIntegerField(db_column='posX')  # Field name made lowercase.
    posy = models.PositiveIntegerField(db_column='posY')  # Field name made lowercase.
    width = models.PositiveIntegerField()
    height = models.PositiveIntegerField()
    alignment = models.CharField(max_length=3)
    caps = models.CharField(max_length=3, blank=True, null=True)
    idkolomtambahan = models.ForeignKey('jbsakad.AkadTambahanData', models.DO_NOTHING, db_column='idkolomtambahan', blank=True, null=True, related_name='+')

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'carddata'


class Cardscandata(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    idscaninfo = models.ForeignKey('Cardscaninfo', models.DO_NOTHING, db_column='idscaninfo')
    nis = models.ForeignKey('jbsakad.Siswa', models.DO_NOTHING, db_column='nis', blank=True, null=True)
    nip = models.ForeignKey('jbssdm.Pegawai', models.DO_NOTHING, db_column='nip', blank=True, null=True)
    nic = models.ForeignKey('jbsakad.CalonSiswa', models.DO_NOTHING, db_column='nic', blank=True, null=True)
    jam = models.DateTimeField()
    keterangan = models.CharField(max_length=255)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'cardscandata'


class Cardscaninfo(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    idaktifitas = models.ForeignKey(Cardaktifitas, models.DO_NOTHING, db_column='idaktifitas')
    informasi = models.CharField(max_length=255)
    mulai = models.DateTimeField()
    selesai = models.DateTimeField(blank=True, null=True)
    petugas = models.CharField(max_length=255)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'cardscaninfo'


class Gambar(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    departemen = models.ForeignKey('jbsakad.Departemen', models.DO_NOTHING, db_column='departemen', blank=True, null=True)
    modul = models.CharField(max_length=45)
    tanggal = models.DateTimeField()
    nama = models.CharField(max_length=255)
    berkas = models.CharField(max_length=255)
    lebar = models.PositiveIntegerField()
    tinggi = models.PositiveIntegerField()
    ukuran = models.PositiveIntegerField()
    lokasi = models.CharField(max_length=255)
    deskripsi = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'gambar'


class Identitas(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=250, blank=True, null=True)
    situs = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    alamat1 = models.CharField(max_length=255, blank=True, null=True)
    alamat2 = models.CharField(max_length=255, blank=True, null=True)
    alamat3 = models.CharField(max_length=255, blank=True, null=True)
    alamat4 = models.CharField(max_length=255, blank=True, null=True)
    telp1 = models.CharField(max_length=20, blank=True, null=True)
    telp2 = models.CharField(max_length=20, blank=True, null=True)
    telp3 = models.CharField(max_length=20, blank=True, null=True)
    telp4 = models.CharField(max_length=20, blank=True, null=True)
    fax1 = models.CharField(max_length=20, blank=True, null=True)
    fax2 = models.CharField(max_length=20, blank=True, null=True)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    foto = models.BinaryField(blank=True, null=True)
    departemen = models.CharField(max_length=50, blank=True, null=True)
    status = models.PositiveIntegerField()
    perpustakaan = models.CharField(max_length=45, blank=True, null=True)
    info1 = models.CharField(max_length=20, blank=True, null=True)
    info2 = models.CharField(max_length=20, blank=True, null=True)
    info3 = models.CharField(max_length=20, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'identitas'


class JenisPekerjaan(JibasUmumModel):
    replid = models.PositiveIntegerField(unique=True)
    pekerjaan = models.CharField(primary_key=True, max_length=100)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'jenispekerjaan'


class LampiranSurat(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    departemen = models.ForeignKey('jbsakad.Departemen', models.DO_NOTHING, db_column='departemen')
    tanggal = models.DateTimeField()
    judul = models.CharField(max_length=255)
    pengantar = models.TextField()
    petugas = models.ForeignKey('jbssdm.Pegawai', models.DO_NOTHING, db_column='petugas', blank=True, null=True)
    aktif = models.PositiveIntegerField()
    info1 = models.CharField(max_length=45, blank=True, null=True)
    info2 = models.CharField(max_length=45, blank=True, null=True)
    info3 = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'lampiransurat'


class PengantarSurat(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    departemen = models.ForeignKey('jbsakad.Departemen', models.DO_NOTHING, db_column='departemen')
    tanggal = models.DateTimeField()
    judul = models.CharField(max_length=255)
    pengantar = models.TextField()
    petugas = models.ForeignKey('jbssdm.Pegawai', models.DO_NOTHING, db_column='petugas', blank=True, null=True)
    aktif = models.PositiveIntegerField()
    info1 = models.CharField(max_length=45, blank=True, null=True)
    info2 = models.CharField(max_length=45, blank=True, null=True)
    info3 = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'pengantarsurat'


class PetaSekolah(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    idsekolah = models.ForeignKey(Identitas, models.DO_NOTHING, db_column='idsekolah')
    idwilayah = models.ForeignKey('jbsumum.Wilayah', models.DO_NOTHING, db_column='idwilayah', blank=True, null=True)
    koordinat = models.CharField(max_length=100, blank=True, null=True)
    gambar = models.TextField(blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'petasekolah'


class Sekolah(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    namasekolah = models.CharField(max_length=45)
    gambar = models.TextField(blank=True, null=True)
    koordinat = models.TextField(blank=True, null=True)
    wilayah = models.ForeignKey('jbsumum.Wilayah', models.DO_NOTHING, db_column='wilayah')
    titikpusat = models.CharField(max_length=10)
    lokasi = models.CharField(max_length=10)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'sekolah'


class Suku(JibasUmumModel):
    replid = models.PositiveIntegerField(unique=True)
    suku = models.CharField(primary_key=True, max_length=20)
    urutan = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'suku'


class Tabledep(JibasUmumModel):
    rootid = models.PositiveIntegerField()
    tname = models.CharField(max_length=100)
    colname = models.CharField(max_length=100, blank=True, null=True)
    deldep = models.CharField(max_length=2, blank=True, null=True)
    upddep = models.CharField(max_length=2, blank=True, null=True)
    tingkat = models.PositiveIntegerField()
    coltype = models.CharField(max_length=100, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'tabledep'


class TingkatPendidikan(JibasUmumModel):
    replid = models.PositiveIntegerField(unique=True)
    pendidikan = models.CharField(primary_key=True, max_length=20)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'tingkatpendidikan'


class Wilayah(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    namawilayah = models.CharField(max_length=45)
    gambar = models.CharField(max_length=100, blank=True, null=True)
    koordinat = models.TextField(blank=True, null=True)
    rootid = models.PositiveIntegerField()
    titikpusat = models.CharField(max_length=15, blank=True, null=True)
    zoom = models.CharField(max_length=30, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'wilayah'


class Wilayah1(JibasUmumModel):
    replid = models.AutoField(primary_key=True)
    namawilayah = models.CharField(max_length=45)
    gambar = models.TextField(blank=True, null=True)
    koordinat = models.TextField(blank=True, null=True)
    rootid = models.PositiveIntegerField()
    titikpusat = models.CharField(max_length=15, blank=True, null=True)
    zoom = models.CharField(max_length=30, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'wilayah1'
