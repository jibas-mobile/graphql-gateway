from django.conf import settings
from django.db import models


class JibasSDMModel(models.Model):
    class Meta:
        abstract = True

    class JibasMeta:
        db_name = 'jbssdm'


class BagianPegawai(JibasSDMModel):
    bagian = models.CharField(primary_key=True, max_length=50)
    urutan = models.PositiveIntegerField()
    replid = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'bagianpegawai'


class Diklat(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    rootid = models.PositiveIntegerField()
    allowselect = models.PositiveIntegerField()
    diklat = models.CharField(max_length=45)
    tingkat = models.PositiveIntegerField()
    jenis = models.CharField(max_length=1)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'diklat'


class Eselon(JibasSDMModel):
    replid = models.PositiveIntegerField(unique=True)
    eselon = models.CharField(primary_key=True, max_length=15)
    urutan = models.PositiveIntegerField()
    isdefault = models.IntegerField(blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'eselon'


class Golongan(JibasSDMModel):
    replid = models.PositiveIntegerField(unique=True)
    golongan = models.CharField(primary_key=True, max_length=14)
    tingkat = models.PositiveIntegerField()
    urutan = models.PositiveIntegerField()
    nama = models.CharField(max_length=100)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'golongan'


class Jabatan(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    rootid = models.PositiveIntegerField()
    jabatan = models.CharField(max_length=255)
    singkatan = models.CharField(max_length=255)
    satker = models.CharField(max_length=255, blank=True, null=True)
    eselon = models.CharField(max_length=15, blank=True, null=True)
    isdefault = models.IntegerField(blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'jabatan'


class JadwalPegawai(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    nip = models.ForeignKey('Pegawai', models.DO_NOTHING, db_column='nip')
    jenis = models.ForeignKey('JenisAgenda', models.DO_NOTHING, db_column='jenis')
    tanggal = models.DateField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    aktif = models.PositiveIntegerField()
    exec = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'jadwal'


class JenisAgenda(JibasSDMModel):
    replid = models.PositiveIntegerField(unique=True)
    agenda = models.CharField(primary_key=True, max_length=45)
    nama = models.CharField(max_length=255)
    urutan = models.IntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'jenisagenda'


class JenisJabatan(JibasSDMModel):
    replid = models.PositiveIntegerField(unique=True)
    jenis = models.CharField(primary_key=True, max_length=50)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    urutan = models.PositiveIntegerField()
    jabatan = models.CharField(max_length=2)
    isdefault = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'jenisjabatan'


class Pegawai(JibasSDMModel):
    replid = models.PositiveIntegerField(unique=True)
    nip = models.CharField(primary_key=True, max_length=30)
    nrp = models.CharField(max_length=30, blank=True, null=True)
    nuptk = models.CharField(max_length=30, blank=True, null=True)
    nama = models.CharField(max_length=100)
    panggilan = models.CharField(max_length=50, blank=True, null=True)
    gelarawal = models.CharField(max_length=45, blank=True, null=True)
    gelarakhir = models.CharField(max_length=45, blank=True, null=True)
    gelar = models.CharField(max_length=50, blank=True, null=True)
    tmplahir = models.CharField(max_length=50, blank=True, null=True)
    tgllahir = models.DateField(blank=True, null=True)
    agama = models.ForeignKey('jbsumum.Agama', models.DO_NOTHING, db_column='agama', blank=True, null=True)
    suku = models.ForeignKey('jbsumum.Suku', models.DO_NOTHING, db_column='suku', blank=True, null=True)
    noid = models.CharField(max_length=50, blank=True, null=True)
    alamat = models.CharField(max_length=255, blank=True, null=True)
    telpon = models.CharField(max_length=20, blank=True, null=True)
    handphone = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    facebook = models.CharField(max_length=200, blank=True, null=True)
    twitter = models.CharField(max_length=200, blank=True, null=True)
    website = models.CharField(max_length=200, blank=True, null=True)
    foto = models.BinaryField(blank=True, null=True)
    bagian = models.ForeignKey(BagianPegawai, models.DO_NOTHING, db_column='bagian')
    nikah = models.CharField(max_length=10)
    keterangan = models.CharField(max_length=200, blank=True, null=True)
    aktif = models.PositiveIntegerField()
    kelamin = models.CharField(max_length=1)
    pinpegawai = models.CharField(max_length=25, blank=True, null=True)
    mulaikerja = models.DateField(blank=True, null=True)
    status = models.CharField(max_length=45, blank=True, null=True)
    ketnonaktif = models.CharField(max_length=45, blank=True, null=True)
    pensiun = models.DateField(blank=True, null=True)
    doaudit = models.IntegerField(blank=True, null=True)
    info1 = models.CharField(max_length=20, blank=True, null=True)
    info2 = models.CharField(max_length=20, blank=True, null=True)
    info3 = models.CharField(max_length=20, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'pegawai'


class PegawaiDiklat(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    nip = models.ForeignKey(Pegawai, models.DO_NOTHING, db_column='nip')
    iddiklat = models.ForeignKey(Diklat, models.DO_NOTHING, db_column='iddiklat')
    tahun = models.PositiveIntegerField()
    sk = models.CharField(max_length=45, blank=True, null=True)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    terakhir = models.PositiveIntegerField()
    doaudit = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'pegdiklat'


class PegawaiGaji(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    nip = models.ForeignKey(Pegawai, models.DO_NOTHING, db_column='nip')
    tanggal = models.DateField()
    gaji = models.CharField(max_length=15)
    sk = models.CharField(max_length=45)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    judulsk = models.CharField(max_length=255, blank=True, null=True)
    tanggalsk = models.CharField(max_length=255, blank=True, null=True)
    dok = models.TextField(blank=True, null=True)
    doaudit = models.IntegerField(blank=True, null=True)
    terakhir = models.IntegerField(blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'peggaji'


class PegawaiGolongan(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    nip = models.ForeignKey(Pegawai, models.DO_NOTHING, db_column='nip')
    golongan = models.ForeignKey(Golongan, models.DO_NOTHING, db_column='golongan')
    tmt = models.DateField()
    sk = models.CharField(max_length=100, blank=True, null=True)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    terakhir = models.PositiveIntegerField()
    judulsk = models.CharField(max_length=255, blank=True, null=True)
    tanggalsk = models.CharField(max_length=45, blank=True, null=True)
    dok = models.TextField(blank=True, null=True)
    petugas = models.CharField(max_length=45, blank=True, null=True)
    doaudit = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'peggol'


class PegawaiJabatan(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    nip = models.ForeignKey(Pegawai, models.DO_NOTHING, db_column='nip')
    idjabatan = models.ForeignKey(Jabatan, models.DO_NOTHING, db_column='idjabatan', blank=True, null=True)
    tmt = models.DateField()
    sk = models.CharField(max_length=45, blank=True, null=True)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    terakhir = models.PositiveIntegerField()
    jenis = models.CharField(max_length=50)
    namajab = models.CharField(max_length=255, blank=True, null=True)
    doaudit = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'pegjab'


class PegawaiKeluarga(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    nip = models.ForeignKey(Pegawai, models.DO_NOTHING, db_column='nip')
    nama = models.CharField(max_length=100, blank=True, null=True)
    alm = models.PositiveIntegerField()
    hubungan = models.CharField(max_length=50, blank=True, null=True)
    tgllahir = models.CharField(max_length=50, blank=True, null=True)
    hp = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'pegkeluarga'


class PegawaiKerja(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    nip = models.ForeignKey(Pegawai, models.DO_NOTHING, db_column='nip')
    tempat = models.CharField(max_length=255)
    thnawal = models.CharField(max_length=4)
    thnakhir = models.CharField(max_length=4)
    jabatan = models.CharField(max_length=255)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    terakhir = models.IntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'pegkerja'


class PegawaiLastData(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    nip = models.ForeignKey(Pegawai, models.DO_NOTHING, db_column='nip')
    idpeggol = models.PositiveIntegerField(blank=True, null=True)
    idpegjab = models.PositiveIntegerField(blank=True, null=True)
    idpegdiklat = models.PositiveIntegerField(blank=True, null=True)
    idpegsekolah = models.PositiveIntegerField(blank=True, null=True)
    idpeggaji = models.PositiveIntegerField(blank=True, null=True)
    idpegserti = models.PositiveIntegerField(blank=True, null=True)
    idpegkerja = models.PositiveIntegerField(blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'peglastdata'


class PegawaiSekolah(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    nip = models.ForeignKey(Pegawai, models.DO_NOTHING, db_column='nip')
    tingkat = models.CharField(max_length=20)
    sekolah = models.CharField(max_length=255)
    lulus = models.PositiveIntegerField()
    sk = models.CharField(max_length=45, blank=True, null=True)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    terakhir = models.PositiveIntegerField()
    doaudit = models.PositiveIntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'pegsekolah'


class PegawaiSertifikat(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    nip = models.ForeignKey(Pegawai, models.DO_NOTHING, db_column='nip')
    sertifikat = models.CharField(max_length=255)
    lembaga = models.CharField(max_length=255)
    tahun = models.SmallIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    terakhir = models.IntegerField()
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'pegserti'


class SDMPilihandata(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    idtambahan = models.ForeignKey('SDMTambahandata', models.DO_NOTHING, db_column='idtambahan')
    pilihan = models.CharField(max_length=255)
    aktif = models.PositiveIntegerField()
    urutan = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'pilihandata'


class Presensi(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    nip = models.ForeignKey(Pegawai, models.DO_NOTHING, db_column='nip')
    tanggal = models.DateField()
    jammasuk = models.CharField(max_length=10, blank=True, null=True)
    jampulang = models.CharField(max_length=10, blank=True, null=True)
    jamwaktukerja = models.PositiveIntegerField(blank=True, null=True)
    menitwaktukerja = models.PositiveIntegerField(blank=True, null=True)
    status = models.PositiveIntegerField()
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    source = models.CharField(max_length=45, blank=True, null=True)
    info1 = models.CharField(max_length=100, blank=True, null=True)
    info2 = models.CharField(max_length=100, blank=True, null=True)
    info3 = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'presensi'


class SatuanKerja(JibasSDMModel):
    replid = models.PositiveIntegerField(unique=True)
    satker = models.CharField(primary_key=True, max_length=255)
    nama = models.CharField(max_length=255)
    isdefault = models.IntegerField(blank=True, null=True)
    info1 = models.CharField(max_length=255, blank=True, null=True)
    info2 = models.CharField(max_length=255, blank=True, null=True)
    info3 = models.CharField(max_length=255, blank=True, null=True)
    ts = models.DateTimeField()
    token = models.PositiveSmallIntegerField()
    issync = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'satker'


class SDMTambahandata(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    kolom = models.CharField(max_length=100)
    keterangan = models.CharField(max_length=255, blank=True, null=True)
    jenis = models.PositiveIntegerField()
    aktif = models.PositiveIntegerField()
    urutan = models.PositiveIntegerField()

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'tambahandata'


class TambahanDataPegawai(JibasSDMModel):
    replid = models.AutoField(primary_key=True)
    nip = models.ForeignKey(Pegawai, models.DO_NOTHING, db_column='nip')
    idtambahan = models.ForeignKey(SDMTambahandata, models.DO_NOTHING, db_column='idtambahan')
    jenis = models.PositiveIntegerField()
    teks = models.CharField(max_length=1000, blank=True, null=True)
    filedata = models.TextField(blank=True, null=True)
    filename = models.CharField(max_length=255, blank=True, null=True)
    filemime = models.CharField(max_length=255, blank=True, null=True)
    filesize = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = settings.JIBAS_TABLE_MANAGED
        db_table = '' if settings.JIBAS_TABLE_MANAGED else 'tambahandatapegawai'
